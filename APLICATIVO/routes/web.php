<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

return Redirect::to('Principal');

});
Route::resource('Principal','PrincipalController');
Route::resource('almacen/categoria','CategoriaController');
Route::resource('almacen/ingredientes','IngredienteController');
Route::resource('almacen/menu','MenuController');
Route::resource('almacen/preparacion','PreparacionController');
Route::resource('almacen/medida','MedidaController');
Route::resource('almacen/receta','Detalle_RecetaController');
Route::resource('almacen/dia','Dia_EspecialController');
Route::resource('almacen/categoria_ingrediente','Categoria_IngredienteController');
Route::resource('almacen/impuesto','ImpuestoController');
Route::resource('almacen/proveedor','ProveedorController');
Route::resource('almacen/tipo_comprobante','TipoComprobanteController');
Route::resource('almacen/cliente','ClienteController');
Route::resource('compras/ingreso','IngresoController');
Route::resource('ventas/venta','VentaController');
Route::resource('almacen/prueba', 'PruebaController');

//Rutas de recuperar
Route::get('recuperar/categoria/{id}', 'CategoriaController@restore');
Route::get('recuperar/ingredientes/{id}', 'IngredienteController@restore');
Route::get('recuperar/menu/{id}', 'MenuController@restore');
Route::get('recuperar/medida/{id}','MedidaController@restore');
Route::get('recuperar/dia/{id}','Dia_EspecialController@restore');
Route::get('recuperar/categoria_ingrediente/{id}','Categoria_IngredienteController@restore');
Route::get('recuperar/impuesto/{id}','ImpuestoController@restore');
Route::get('recuperar/proveedor/{id}','ProveedorController@restore');
Route::get('recuperar/tipo_comprobante/{id}','TipoComprobanteController@restore');
Route::get('recuperar/cliente/{id}','ClienteController@restore');

//crear receta
Route::get('Principal','PrincipalController@app');

Route::get('detalle_receta/{id}','PreparacionController@create');
Route::get('crear/receta/{id}','Detalle_RecetaController@create');
Route::get('crear/receta/prueba/{id}','Detalle_RecetaController@browingr');
Route::get('preparacion/detalle','PreparacionController@indexpreparaciones');
Route::get('receta/detalle/','Detalle_RecetaController@indexreceta');



//rutas de eliminar detalle de la receta
Route::get('almacen/eliminar_detalle/{id}','Detalle_RecetaController@eliminardetalle');



//RUTAS para pdf con detalles

Route::get('reportedetallereceta/{id}','Detalle_RecetaController@pdfdetalle');
Route::get('reportedetallepreparacion/{id}','PreparacionController@pdfdetalle');

//rutas de login
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//rutas crear cliente en interfaz venta
Route::get('cliente/venta','VentaController@cliente');














