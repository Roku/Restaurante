<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Categoria_Ingrediente;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\MenuFormRequest;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;
use Laracasts\Flash\Flash;

class Categoria_IngredienteController extends Controller
{
    
    public function index(Request $request)
    {
        if ($request) {
            $categoria_ingrediente=DB::table('categoria_ingrediente')
            ->get();

            $impuesto=DB::table('impuesto')
            ->where('estado','=','Activo')
            ->get();
            return view('almacen.categoria_ingrediente.index',["categoria_ingrediente"=>$categoria_ingrediente,"impuesto"=>$impuesto]);
        }
    }

    
    public function create()
    {
        //
    }

  
    public function store(Request $request)
    {
        $categoria_ingrediente=new Categoria_Ingrediente;
        $categoria_ingrediente->idimpuesto=$request->get('idimpuesto');
        $categoria_ingrediente->nombre=$request->get('nombre');
        $categoria_ingrediente->descripcion=$request->get('descripcion');
        $categoria_ingrediente->estado="Activo";
        $categoria_ingrediente->save();
     Flash::success('La categoría '.$categoria_ingrediente->nombre.' ha sido guardada exitosamente!');
        return Redirect::to('almacen/categoria_ingrediente');   
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        $categoria_ingrediente=Categoria_Ingrediente::findOrFail($id);
        $categoria_ingrediente->idimpuesto=$request->get('idimpuesto');
        $categoria_ingrediente->nombre=$request->get('nombre');
        $categoria_ingrediente->descripcion=$request->get('descripcion');
        $categoria_ingrediente->update();
      Flash::success('La categoría '.$categoria_ingrediente->nombre.' ha sido actualizada exitosamente!');
     return Redirect::to('almacen/categoria_ingrediente');
    }

   
      public function destroy($id)
    {
        $categoria_ingrediente=Categoria_Ingrediente::findOrFail($id);
        $categoria_ingrediente->estado='Inactivo';
        $categoria_ingrediente->update();
              Flash::success('La categoría '.$categoria_ingrediente->nombre.' ha sido eliminada exitosamente!');
         return Redirect::to('almacen/categoria_ingrediente');
    }

    public function restore($id)
    {
       $categoria_ingrediente=Categoria_Ingrediente::findOrFail($id);
       $categoria_ingrediente->estado='Activo';
       $categoria_ingrediente->update();
             Flash::success('La categoría '.$categoria_ingrediente->nombre.' ha sido recuperada exitosamente!');
       return Redirect::to('almacen/categoria_ingrediente');
    }
}
