<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Menu;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\MenuFormRequest;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;
use Laracasts\Flash\Flash;

class MenuController extends Controller
{
    public function __construct($value='')
    {
        # code...
    }
   
    public function index(Request $request)
    {
        if ($request) {
            $menu=DB::table('menu as m')
            ->join('categoria as c','m.idcategoria','=','c.idcategoria')
            ->select('m.idmenu','c.idcategoria','m.nombre','precio_venta','m.descripcion','m.fecha','m.estado','m.imagen','c.nombre as categoria')
            ->get();

            $categoria=DB::table('categoria')
            ->where('estado','=','Activo')
            ->get();

            return view('almacen.menu.index',["menu"=>$menu,"categoria"=>$categoria]);

        }
    }


    public function create()
    {
        
    }

 
    public function store(MenuFormRequest $request)
    {
        
  $menu=new Menu;
  $menu->idcategoria=$request->get('idcategoria');
  $menu->nombre=$request->get('nombre');
  $menu->precio_venta=$request->get('precio_venta');
  $menu->descripcion=$request->get('descripcion');
  $mytime=Carbon::now('America/Guayaquil');
  $menu->fecha=$mytime->toDateString();

     if (Input::hasFile('imagen')) {
           $file=Input::file('imagen');
           $file->move(public_path().'/imagenes/menu/',$file->getClientOriginalName());
           $menu->imagen=$file->getClientOriginalName();

         
      }
        $menu->estado="Activo";
  $menu->save();
  Flash::success('El menu '.$menu->nombre.' ha sido guardado  exitosamente!');
  return Redirect::to('almacen/menu');



    }

   
    public function show($id)
    {
        //
    }

  
    public function edit($id)
    {
        //
    }

 
    public function update(MenuFormRequest $request, $id)
    {
         $menu=Menu::findOrFail($id);
  $menu->idcategoria=$request->get('idcategoria');
  $menu->nombre=$request->get('nombre');
  $menu->precio_venta=$request->get('precio_venta');
  $menu->descripcion=$request->get('descripcion');
  $mytime=Carbon::now('America/Guayaquil');
  $menu->fecha=$mytime->toDateString();

     if (Input::hasFile('imagen')) {
           $file=Input::file('imagen');
           $file->move(public_path().'/imagenes/menu/',$file->getClientOriginalName());
           $menu->imagen=$file->getClientOriginalName();

         
      }
        $menu->update();
        Flash::success('El menu '.$menu->nombre.' ha sido actualizado exitosamente!');
        return Redirect::to('almacen/menu');
    }

  
    public function destroy($id)
    {
        $menu=Menu::findOrFail($id);
        $menu->estado='Inactivo';
        $menu->update();
        Flash::error('El menu '.$menu->nombre.' ha sido eliminado exitosamente!');
         return Redirect::to('almacen/menu');
    }
     public function restore($id)
    {
       $menu=Menu::findOrFail($id);
       $menu->estado='Activo';
       $menu->update();
       Flash::warning('El menu '.$menu->nombre.' ha sido recuperado exitosamente!');
       return Redirect::to('almacen/menu');
    }
}
