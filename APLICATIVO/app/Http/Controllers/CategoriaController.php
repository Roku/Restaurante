<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Categoria;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\CategoriaFormRequest;
use DB;
use Laracasts\Flash\Flash;


class CategoriaController extends Controller
{
 
 //pra que no puedan ingresar sin logearce
public function __construct($value='')
{
 
}


//funcion para la precentacion de mis datos en tabla
    public function index(Request $request)
    {
       
     if ($request) {
         $categoria=DB::table('categoria')
         ->orderBy('idcategoria','desc')
         ->get();

         return view('almacen.categoria.index',["categoria"=>$categoria]);
     }

    }

    public function store(CategoriaFormRequest $request)
    {
        $categoria=new Categoria;
        $categoria->nombre=$request->get('nombre');
        $categoria->descripcion=$request->get('descripcion');
        $categoria->estado="Activo";
        $categoria->save();
     Flash::success('La categoría '.$categoria->nombre.' ha sido guardada exitosamente!');
        return Redirect::to('almacen/categoria');   
    }

  
    public function show($id)
    {
        //
    }

    public function update(CategoriaFormRequest $request, $id)
    {
        
        $categoria=Categoria::findOrFail($id);
        $categoria->nombre=$request->get('nombre');
        $categoria->descripcion=$request->get('descripcion');
        $categoria->update();
      Flash::success('La categoría '.$categoria->nombre.' ha sido actualizada exitosamente!');
     return Redirect::to('almacen/categoria');
    }

  
    public function destroy($id)
    {
        $categoria=Categoria::findOrFail($id);
        $categoria->estado='Inactivo';
        $categoria->update();
              Flash::success('La categoría '.$categoria->nombre.' ha sido eliminada exitosamente!');
         return Redirect::to('almacen/categoria');
    }

    public function restore($id)
    {
       $categoria=Categoria::findOrFail($id);
       $categoria->estado='Activo';
       $categoria->update();
             Flash::success('La categoría '.$categoria->nombre.' ha sido recuperada exitosamente!');
       return Redirect::to('almacen/categoria');
    }
}
