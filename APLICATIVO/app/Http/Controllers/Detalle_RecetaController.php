<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Detalle_RecetaFormRequest;
use App\Http\Requests\RecetaFormRequest;

use App\Detalle_Receta;
use App\Categoria_Ingrediente;
use App\Ingredientes;
use App\Receta;
use App\Ingrediente;

use DB;
use Response;
use Illuminate\Support\Collection;
use Laracasts\Flash\Flash;
use PDF;


class Detalle_RecetaController extends Controller
{
       public function __construct(){



    }
        public function getTowns(Request $request, $id)
    {
        if ($request->ajax()) {
            $categoria_ingrediente=Categoria_Ingrediente::categoria_ingrediente($id);
            return response()->json($categoria_ingrediente);
        }
    }

    public function index(Request $request)
    {
       if ($request) {

            $receta=DB::table('receta as r')
            ->join('menu as m','m.idmenu','=','r.idmenu')
            ->select('r.idreceta','m.idmenu','m.nombre','r.coccion','r.peso_receta','r.peso_porcion','r.porciones','r.estado')
            ->get();

           $categoria=DB::table('categoria')
            ->where('estado','=','Activo')
            ->get();

            return view('almacen.receta.index',["receta"=>$receta,"categoria"=>$categoria]);

        }
    }

    public function indexreceta(Request $request)
    {
        if ($request) {
            
            $menu=DB::table('menu as m')
            ->join('categoria as c','m.idcategoria','=','c.idcategoria')
            ->select('m.idmenu','c.idcategoria','m.nombre as menu','m.descripcion as nota','m.fecha','m.estado','c.nombre as categoria')
            ->get();

            $categoria=DB::table('categoria')
            ->where('estado','=','Activo')
            ->get();

            return view('almacen.receta.indexreceta',["menu"=>$menu,"categoria"=>$categoria]);

        }
    }

  
    public function create($id)
    {
      $categ=Categoria_Ingrediente::pluck('nombre','idcategoria_ingrediente');
    $menu=DB::table('menu as m')
    ->select('m.idmenu','m.nombre as menu')
    ->where('m.idmenu','=',$id)
    ->first(); 
    $categoria_ingrediente=DB::table('categoria_ingrediente') 
    ->where('estado','=','Activo')->get();  

 $prueba=DB::table('categoria_ingrediente as ci')
 ->join('ingrediente as i','ci.idcategoria_ingrediente','=','i.idcategoria_ingrediente')
 ->select('ci.idcategoria_ingrediente','i.idingrediente','i.nombre','ci.nombre as categoria')
 ->get();


    $ingrediente=DB::table('ingrediente') ->where('estado','=','Activo')->get();
    
      $medida=DB::table('unidad_medida')->where('estado','=','Activo')->get();
 
      return view('almacen.receta.create',compact('categ'),["ingrediente"=>$ingrediente,"medida"=>$medida,"menu"=>$menu,"categoria_ingrediente"=>$categoria_ingrediente,"prueba"=>$prueba]);
    }




  public function browingr(Request $request,$id)
  {
    if($request->ajax()){
      $ingredient = Ingredientes::ingred($id);
      return response()->json($ingredient);
    }
  }
    



    public function store(RecetaFormRequest $request)
    {
     

 DB::beginTransaction();
$receta= new Receta;
$receta->idmenu=$request->get('idmenu');    
$receta->coccion=$request->get('coccion');
$receta->peso_receta=$request->get('peso_receta');
$receta->peso_porcion=$request->get('peso_porcion');
$receta->porciones=$request->get('porciones');
$receta->estado="Activo";
$receta->save();



$idingrediente =$request->get('idingrediente');
$idunidad_medida=$request->get('idunidad_medida');
$cantidad=$request->get('cantidad');
$cont=0;

while( $cont < count($idingrediente)) {
    
  $detalle=new Detalle_Receta();
$detalle->idreceta=$receta->idreceta;
$detalle->idingrediente=$idingrediente[$cont];
$detalle->idunidad_medida=$idunidad_medida[$cont];
$detalle->cantidad= $cantidad[$cont];

//$detalle->impuesto=$impuesto[$cont]; 


$detalle->save();
$cont=$cont+1;

}
      DB::commit();

Flash::success('La Receta ha sido guardada exitosamente!');
        return Redirect::to('almacen/receta'); 

     return Redirect::to('almacen/receta');
}
  
    public function show($id)
    {
      $receta=DB::table('receta as r')
->join('menu as m','m.idmenu','=','r.idmenu')
->select('r.idreceta','m.nombre','r.coccion','r.peso_receta','r.peso_porcion','r.porciones')
->where('r.idmenu','=',$id)
->first();     
      $ingrediente=DB::table('ingrediente') ->where('estado','=','Activo')->get();
    
      $medida=DB::table('unidad_medida')->where('estado','=','Activo')->get();

      $detalles=DB::table('detalle_receta as dt')
      ->join('ingrediente as i','i.idingrediente','=','dt.idingrediente')
      ->join('receta as r','r.idreceta','=','dt.idreceta')
      ->join('unidad_medida as um','um.idunidad_medida','=','dt.idunidad_medida')
      ->join('menu as m','m.idmenu','=','r.idmenu')
      ->select('dt.iddetalle_receta as iddetalle_receta','r.idmenu','i.nombre','dt.cantidad','m.nombre as menu','um.nombre as medida')
      ->where('r.idmenu','=',$id)
      ->get();
 
      return view('almacen.receta.show',["ingrediente"=>$ingrediente,"medida"=>$medida,"receta"=>$receta,"detalles"=>$detalles]);
    }

    
    public function edit($id)
    {
    
$receta=DB::table('receta as r')
->join('menu as m','m.idmenu','=','r.idmenu')
->select('r.idreceta','m.nombre','r.coccion','r.peso_receta','r.peso_porcion','r.porciones')
->where('r.idmenu','=',$id)
->first();     
    $ingrediente=DB::table('ingrediente') ->where('estado','=','Activo')->get();
    
      $medida=DB::table('unidad_medida')->where('estado','=','Activo')->get();

      $detalles=DB::table('detalle_receta as dt')
      ->join('ingrediente as i','i.idingrediente','=','dt.idingrediente')
      ->join('receta as r','r.idreceta','=','dt.idreceta')
      ->join('unidad_medida as um','um.idunidad_medida','=','dt.idunidad_medida')
      ->join('menu as m','m.idmenu','=','r.idmenu')
      ->select('dt.iddetalle_receta','r.idmenu','i.nombre','dt.cantidad','m.nombre as menu','um.nombre as medida')
      ->where('r.idmenu','=',$id)
      ->get();
 
      return view('almacen.receta.edit',["ingrediente"=>$ingrediente,"medida"=>$medida,"receta"=>$receta,"detalles"=>$detalles]);
    }

    
    public function update(RecetaFormRequest $request, $id)
    {
       DB::beginTransaction();
$receta=Receta::findOrFail($id);
   
$receta->coccion=$request->get('coccion');
$receta->peso_receta=$request->get('peso_receta');
$receta->peso_porcion=$request->get('peso_porcion');
$receta->porciones=$request->get('porciones');
$receta->update();



$idingrediente =$request->get('idingrediente');
$idunidad_medida=$request->get('idunidad_medida');
$cantidad=$request->get('cantidad');
$cont=0;

while( $cont < count($idingrediente)) {
    
  $detalle=new Detalle_Receta();
$detalle->idreceta=$receta->idreceta;
$detalle->idingrediente=$idingrediente[$cont];
$detalle->idunidad_medida=$idunidad_medida[$cont];
$detalle->cantidad= $cantidad[$cont];

//$detalle->impuesto=$impuesto[$cont]; 


$detalle->save();
$cont=$cont+1;

}
      DB::commit();

Flash::success('La Receta ha sido guardada exitosamente!');
        return Redirect::to('almacen/receta'); 

     return Redirect::to('almacen/receta'); 
    }

   
    public function destroy($id)
    {
   
    }


    public function eliminardetalle($id)
    {
      $detalle=Detalle_Receta::findOrFail($id);
       $detalle->delete();
      return Redirect::back();
    }
    public function pdfdetalle($id)
    {
     $menu=DB::table('menu as m')
->select('m.idmenu','m.nombre as menu')
->where('m.idmenu','=',$id)
->first();      
    $ingrediente=DB::table('ingrediente') ->where('estado','=','Activo')->get();
    
      $medida=DB::table('unidad_medida')->where('estado','=','Activo')->get();

      $detalles=DB::table('detalle_receta as dt')
      ->join('ingrediente as i','i.idingrediente','=','dt.idingrediente')
      ->join('menu as m','m.idmenu','=','dt.idmenu')
      ->join('unidad_medida as um','um.idunidad_medida','=','dt.idunidad_medida')
      ->select('dt.iddetalle_receta','m.idmenu','i.nombre','dt.cantidad','m.nombre as menu','um.nombre as medida')
      ->where('m.idmenu','=',$id)
      ->get();

       $pdf = PDF::loadView('almacen.receta.pdfdetalle', ["menu"=>$menu,"medida"=>$medida,"detalles"=>$detalles,"ingrediente"=>$ingrediente]);
return $pdf->download('archivo.pdf'.$id.'.pdf');
    }

}
