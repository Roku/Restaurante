<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Proveedor;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ProveedorFormRequest;
use DB;
use Laracasts\Flash\Flash;
class ProveedorController extends Controller
{
    
    public function index(Request $request)
    {
        if ($request) {
         $proveedor=DB::table('proveedor')
         ->orderBy('idproveedor','desc')
         ->get();

         return view('almacen.proveedor.index',["proveedor"=>$proveedor]);
     }
    }

    
    public function store(Request $request)
    {
        $proveedor=new Proveedor;
        $proveedor->empresa=$request->get('empresa');
        $proveedor->nombre=$request->get('nombre');
        $proveedor->apellido=$request->get('apellido');
        $proveedor->num_documento=$request->get('num_documento');
        $proveedor->telefono=$request->get('telefono');
        $proveedor->direccion=$request->get('direccion');
        $proveedor->email=$request->get('email');
        $proveedor->estado="Activo";
        $proveedor->save();
     Flash::success('El proveedor '.$proveedor->nombre.' ha sido guardada exitosamente!');
        return Redirect::to('almacen/proveedor');   
    }


    
    public function update(ProveedorFormRequest $request, $id)
    {
        $proveedor=Proveedor::findOrFail($id);    
        $proveedor->empresa=$request->get('empresa');
        $proveedor->nombre=$request->get('nombre');
        $proveedor->apellido=$request->get('apellido');
        $proveedor->num_documento=$request->get('num_documento');
        $proveedor->telefono=$request->get('telefono');
        $proveedor->direccion=$request->get('direccion');
        $proveedor->email=$request->get('email');
        $proveedor->update();
     Flash::success('El proveedor '.$proveedor->nombre.' ha sido actualizado exitosamente!');
        return Redirect::to('almacen/proveedor');   
     }

   
      public function destroy($id)
    {
        $proveedor=Proveedor::findOrFail($id);
        $proveedor->estado='Inactivo';
        $proveedor->update();
              Flash::success('El proveedor '.$proveedor->nombre.' ha sido eliminado exitosamente!');
         return Redirect::to('almacen/proveedor');
    }

    public function restore($id)
    {
       $proveedor=Proveedor::findOrFail($id);
       $proveedor->estado='Activo';
       $proveedor->update();
             Flash::success('El proveedor '.$proveedor->nombre.' ha sido recuperada exitosamente!');
       return Redirect::to('almacen/proveedor');
    }
}
