<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Preparacion;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\PreparacionFormRequest;
use DB;
use Laracasts\Flash\Flash;
use PDF;

class PreparacionController extends Controller
{

    public function __construct($value='')
    {
        
    }
    
    public function index()
    {
         
 $menu=DB::table('preparacion as p')
            ->join('menu as m','m.idmenu','=','p.idmenu')
            ->select('m.idmenu','m.nombre as menu','p.descripcion as nota','p.estado','p.idpreparacion')
            ->get();

 $categoria=DB::table('categoria')
            ->where('estado','=','Activo')
            ->get();

         
            return view('almacen.preparacion.index',["menu"=>$menu,"categoria"=>$categoria]);
    }


    public function indexpreparaciones()
    {

 $menu=DB::table('menu as m')
            ->join('categoria as c','m.idcategoria','=','c.idcategoria')
            ->select('m.idmenu','c.idcategoria','m.nombre','m.descripcion','m.fecha','m.estado','c.nombre as categoria')
            ->where('m.estado','=',"Activo")
            ->get();

 $categoria=DB::table('categoria')
            ->where('estado','=','Activo')
            ->get();

         return view('almacen.preparacion.indexpreparaciones',["menu"=>$menu,"categoria"=>$categoria]);
}

    
    public function create($id)
    {
         $menu=DB::table('menu as m')
            ->join('categoria as c','m.idcategoria','=','c.idcategoria')
            ->select('m.idmenu','c.idcategoria','m.nombre as menu','m.descripcion','m.fecha','m.estado','c.nombre as categoria')
           ->where('m.idmenu','=',$id)
           ->first();
         $detalles=DB::table('detalle_receta as d')
           ->join('ingrediente as i','d.idingrediente','=','i.idingrediente')
           ->join('unidad_medida as u','u.idunidad_medida','=','d.idunidad_medida')
          ->join('receta as r','r.idreceta','=','d.idreceta')
          ->join('menu as m','m.idmenu','=','r.idmenu')
    ->select('d.iddetalle_receta','m.idmenu','i.nombre as ingrediente','d.cantidad','u.nombre as medida')
           ->where('m.idmenu','=',$id)
           ->get();
         
           
  return view("almacen.preparacion.create",["detalles"=>$detalles,"menu"=>$menu]);


    }

 
    public function store(PreparacionFormRequest $request)
    {
        $preparacion=new Preparacion;
        $preparacion->idmenu=$request->get('idmenu');
        $preparacion->preparacion=$request->get('preparacion');
        $preparacion->descripcion=$request->get('descripcion');
        $preparacion->estado="Activo";
        $preparacion->save();
        Flash::success('La preparacion '.$preparacion->nombre.' ha sido guardada exitosamente!');
        return Redirect::to('almacen/preparacion');
    }


    public function show($id)
    {
        
$menu=DB::table('menu as m')
            ->join('categoria as c','m.idcategoria','=','c.idcategoria')
            ->select('m.idmenu','c.idcategoria','m.nombre as menu','m.descripcion','m.fecha','m.estado','c.nombre as categoria')
           ->where('m.idmenu','=',$id)
           ->first();
   $preparacion=DB::table('preparacion')
  ->select('idpreparacion','idmenu','preparacion','descripcion','estado')
  ->where('idmenu','=',$id)
  ->first();
  

        $detalles=DB::table('detalle_receta as d')
           ->join('ingrediente as i','d.idingrediente','=','i.idingrediente')
           ->join('unidad_medida as u','u.idunidad_medida','=','d.idunidad_medida')
          ->join('receta as r','r.idreceta','=','d.idreceta')
          ->join('menu as m','m.idmenu','=','r.idmenu')
          ->select('d.iddetalle_receta','m.idmenu','i.nombre as ingrediente','d.cantidad','u.nombre as medida')
           ->where('m.idmenu','=',$id)
           ->get();
         
           
  return view("almacen.preparacion.show",["detalles"=>$detalles,"menu"=>$menu,"preparacion"=>$preparacion]);




    }

 
    public function edit($id)
    {



      $menu=DB::table('menu as m')
            ->join('categoria as c','m.idcategoria','=','c.idcategoria')
            ->select('m.idmenu','c.idcategoria','m.nombre as menu','m.descripcion','m.fecha','m.estado','c.nombre as categoria')
           ->where('m.idmenu','=',$id)
           ->first();

            $preparacion=DB::table('preparacion')
  ->select('idpreparacion','idmenu','preparacion','descripcion','estado')
  ->where('idmenu','=',$id)
  ->first();
  

           $detalles=DB::table('detalle_receta as d')
           ->join('ingrediente as i','d.idingrediente','=','i.idingrediente')
           ->join('unidad_medida as u','u.idunidad_medida','=','d.idunidad_medida')
          ->join('receta as r','r.idreceta','=','d.idreceta')
          ->join('menu as m','m.idmenu','=','r.idmenu')
          ->select('d.iddetalle_receta','m.idmenu','i.nombre as ingrediente','d.cantidad','u.nombre as medida')
           ->where('m.idmenu','=',$id)
           ->get();
 return view('almacen.preparacion.edit',["menu"=>$menu,"preparacion"=>$preparacion,"detalles"=>$detalles]);
    }

   
    public function update(PreparacionFormRequest $request, $id)
    {
       
        $preparacion=Preparacion::findOrFail($id);
        $preparacion->preparacion=$request->get('preparacion');
        $preparacion->descripcion=$request->get('descripcion');
        $preparacion->update();
        Flash::success('La preparacion '.$preparacion->nombre.' ha sido actualizada exitosamente!');
        return Redirect::to('almacen/preparacion');
    }

  
    public function destroy($id)
    {
        $preparacion=Preparacion::findOrFail($id);
        $preparacion->delete();
        Flash::error('La preparacion '.$preparacion->nombre.' ha sido eliminada exitosamente!');
            return Redirect::to('almacen/preparacion');
    }


 public function pdfdetalle($id)
 {

 $menu=DB::table('menu as m')
            ->join('categoria as c','m.idcategoria','=','c.idcategoria')
            ->select('m.idmenu','c.idcategoria','m.nombre as menu','m.descripcion','m.fecha','m.estado','c.nombre as categoria')
           ->where('m.idmenu','=',$id)
           ->first();
   $preparacion=DB::table('preparacion')
  ->select('idpreparacion','idmenu','preparacion','descripcion','estado')
  ->where('idmenu','=',$id)
  ->first();
  

          $detalles=DB::table('detalle_receta as d')
           ->join('ingrediente as i','d.idingrediente','=','i.idingrediente')
           ->join('unidad_medida as u','u.idunidad_medida','=','d.idunidad_medida')
          ->join('receta as r','r.idreceta','=','d.idreceta')
          ->join('menu as m','m.idmenu','=','r.idmenu')
          ->select('d.iddetalle_receta','m.idmenu','i.nombre as ingrediente','d.cantidad','u.nombre as medida')
           ->where('m.idmenu','=',$id)
           ->get();

            $pdf = PDF::loadView('almacen.preparacion.pdfdetalle', ["menu"=>$menu,"preparacion"=>$preparacion,"detalles"=>$detalles]);
return $pdf->download('archivo.pdf'.$id.'.pdf');
 }

    
}
