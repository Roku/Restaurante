<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Ingredientes;
use Illuminate\Support\Facades\Redirect;
use App\Http\Request\IngredientesFormReques;
use DB;
use Laracasts\Flash\Flash;

class IngredienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $ingrediente=DB::table('ingrediente as i')
            ->join('categoria_ingrediente as ci','ci.idcategoria_ingrediente','=','i.idcategoria_ingrediente')
           ->select('i.idingrediente','i.idcategoria_ingrediente','i.nombre','i.descripcion','ci.nombre as categoria','i.estado')
            ->get();

             $categoria_ingrediente=DB::table('categoria_ingrediente')
            ->where('estado','=','Activo')
            ->get();

            return view('almacen.ingredientes.index',["ingrediente"=>$ingrediente,"categoria_ingrediente"=>$categoria_ingrediente]);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //primero crear una variable
        $ingrediente = new Ingredientes;
        //tomamos el name de nuestro html en este caso
        //de nuestro modal que contiene el campo
        //input name='nombre' para guardar el nombre y asi los demas
        $ingrediente->idcategoria_ingrediente=$request->get('idcategoria_ingrediente');
        $ingrediente->nombre=$request->get('nombre');
        $ingrediente->descripcion=$request->get('descripcion');
        $ingrediente->estado="Activo";
        $ingrediente->save();
        Flash::success('El ingrediente '.$ingrediente->nombre.' ha sido actualizada exitosamente!');
        return Redirect::to('almacen/ingredientes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ingrediente=Ingredientes::findOrFail($id);
        $ingrediente->idcategoria_ingrediente=$request->get('idcategoria_ingrediente');
        $ingrediente->nombre=$request->get('nombre');
        $ingrediente->descripcion=$request->get('descripcion');
        $ingrediente->update();
        Flash::success('El ingrediente '.$ingrediente->nombre.' ha sido actualizada exitosamente!');
        return Redirect::to('almacen/ingredientes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ingrediente=Ingredientes::findOrFail($id);
        $ingrediente->estado='Inactivo';
        $ingrediente->update();
        Flash::error('El ingrediente '.$ingrediente->nombre.' ha sido eliminada exitosamente!');
            return Redirect::to('almacen/ingredientes');

    }
    public function restore($id)
    {
        $ingrediente=Ingredientes::findOrFail($id);
        $ingrediente->estado='Activo';
        $ingrediente->update();
        Flash::warning('El ingrediente '.$ingrediente->nombre.' ha sido recuperada exitosamente!');
        return Redirect::to('almacen/ingredientes');
    }
}
