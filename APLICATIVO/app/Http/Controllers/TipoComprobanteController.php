<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Tipo_Comprobante;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\TipoComprobanteFormRequest;
use DB;
use Laracasts\Flash\Flash;

class TipoComprobanteController extends Controller
{
   
    public function index(Request $request)
    {
        if ($request) {
         $tipo_Comprobante=DB::table('tipo_comprobante')
         ->orderBy('idtipo_comprobante','desc')
         ->get();

         return view('almacen.tipo_comprobante.index',["tipo_Comprobante"=>$tipo_Comprobante]);
     }
    }

    

    
    public function store(Request $request)
    {
     $tipo_Comprobante=new Tipo_Comprobante;
        $tipo_Comprobante->nombre=$request->get('nombre');
        $tipo_Comprobante->descripcion=$request->get('descripcion');
        $tipo_Comprobante->estado="Activo";
        $tipo_Comprobante->save();
     Flash::success('El comprobante '.$tipo_Comprobante->nombre.' ha sido guardada exitosamente!');
        return Redirect::to('almacen/tipo_comprobante');   
    }

   
    public function update(TipoComprobanteFormRequest $request, $id)
    {
         
        $tipo_Comprobante=Tipo_Comprobante::findOrFail($id);
        $tipo_Comprobante->nombre=$request->get('nombre');
        $tipo_Comprobante->descripcion=$request->get('descripcion');
        $tipo_Comprobante->update();
      Flash::success('El comprobante '.$tipo_Comprobante->nombre.' ha sido actualizada exitosamente!');
     return Redirect::to('almacen/tipo_comprobante');
    }

    
    public function destroy($id)
    {
        $tipo_Comprobante=Tipo_Comprobante::findOrFail($id);
        $tipo_Comprobante->estado='Inactivo';
        $tipo_Comprobante->update();
              Flash::success('El comprobante'.$tipo_Comprobante->nombre.' ha sido eliminada exitosamente!');
         return Redirect::to('almacen/tipo_comprobante');
    }

    public function restore($id)
    {
       $tipo_Comprobante=Tipo_Comprobante::findOrFail($id);
       $tipo_Comprobante->estado='Activo';
       $tipo_Comprobante->update();
             Flash::success('El comprobante '.$tipo_Comprobante->nombre.' ha sido recuperada exitosamente!');
       return Redirect::to('almacen/tipo_comprobante');
    }
}
