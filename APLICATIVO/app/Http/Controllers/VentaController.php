<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\VentaFormRequest;
use App\Venta;
use App\Detalle_Venta;
use App\Cliente;
use DB;
use App\Http\Requests\ClienteFormRequest;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use PDF;
use Laracasts\Flash\Flash;

class VentaController extends Controller
{
   
    public function index(Request $request)
    {
if ($request) {
	$venta=DB::table('venta as v')
	->join('cliente as cli','cli.idcliente','=','v.idcliente')
	->join('tipo_comprobante as tc','tc.idtipo_comprobante','=','v.idtipo_comprobante')
	->select('v.idventa','cli.nombre','cli.apellido','cli.num_documento','v.num_comprobante','v.fecha_hora','v.subtotal_venta','v.iva_venta','v.total_venta','v.estado')
	->groupBy('v.idventa','cli.nombre','cli.apellido','cli.num_documento','v.num_comprobante','v.fecha_hora','v.subtotal_venta','v.iva_venta','v.total_venta','v.estado')
	->get();
	  return view('ventas.venta.index',["venta"=>$venta]);
}
         

    }

    
    public function create()
    {
        

 $categoria=DB::table('categoria as c')
            ->select('idcategoria as categoria','nombre','descripcion')
            ->where('estado','=','Activo')
            ->get();

            $impuesto=DB::table('impuesto')
            ->where('impuesto','>','0')
            ->get();

        $cliente=DB::table('cliente') ->where('estado','=','Activo')->get();
    
      $tipo_comprobante=DB::table('tipo_comprobante')->where('estado','=','Activo')->get();
       $medida=DB::table('unidad_medida')->where('estado','=','Activo')->get();

       $menu=DB::table('menu as m')
       ->join('categoria as c','c.idcategoria','=','m.idcategoria')
       ->select('m.idmenu','c.nombre as categoria','c.idcategoria as c','m.nombre as menu','m.precio_venta','m.imagen','m.fecha','m.estado')
       ->where('m.estado','=','Activo')
       ->get();

  return view('ventas.venta.create',["cliente"=>$cliente,"menu"=>$menu,"tipo_comprobante"=>$tipo_comprobante,"medida"=>$medida,"categoria"=>$categoria,"impuesto"=>$impuesto]);


    }

   
    public function store(VentaFormRequest $request)
    {
         
          DB::beginTransaction();
$venta= new Venta;
$venta->idcliente=$request->get('idcliente');    

$venta->idtipo_comprobante=$request->get('idtipo_comprobante');
$venta->num_comprobante=$request->get('num_comprobante');
$mytime=Carbon::now('America/Guayaquil');
$venta->fecha_hora=$mytime->toDateTimeString();
$venta->subtotal_venta=$request->get('subtotal_venta');
$venta->iva_venta=$request->get('iva_venta');
$venta->total_venta=$request->get('total_venta');
$venta->estado='Activo';
$venta->save();


$idmenu =$request->get('idmenu');
$cantidad =$request->get('cantidad');
$precio_venta=$request->get('precio_venta');
//$impuesto=$request->get('impuesto');

$cont=0;

while( $cont < count($idmenu)) {
    
    $detalle= new Detalle_Venta();

$detalle->idventa=$venta->idventa;
$detalle->idmenu=$idmenu[$cont];
$detalle->cantidad= $cantidad[$cont];
$detalle->precio_venta=  $precio_venta[$cont];
//$detalle->impuesto=$impuesto[$cont]; 
$detalle->save();
$cont=$cont+1;



}



          DB::commit();

       
      Flash::success('Venta exitosamente!');
        
     return Redirect::to('ventas/venta');
    }

   
    public function show($id)
    {

    	$venta=DB::table('venta as v')
	->join('cliente as cli','cli.idcliente','=','v.idcliente')
	->join('tipo_comprobante as tc','tc.idtipo_comprobante','=','v.idtipo_comprobante')
	->select('v.idventa','cli.nombre','cli.apellido','cli.num_documento','v.num_comprobante','v.fecha_hora','v.subtotal_venta','v.iva_venta','v.total_venta','v.estado','tc.nombre as comprobante','cli.direccion','cli.telefono')
	->where('v.idventa','=',$id)
	->first();

        
           $detalles=DB::table('detalle_venta as d')
           ->join('venta as v','v.idventa','=','d.idventa')
           ->join('menu as m','m.idmenu','d.idmenu')
           ->select('d.idventa','m.nombre as menu','d.cantidad','d.precio_venta')
           ->where('d.idventa','=',$id)
           ->get();


           return view("ventas.venta.show",["venta"=>$venta,"detalles"=>$detalles]);
   


    }

    
    public function cliente(ClienteFormRequest $request)
    {
        $cliente=new Cliente;
        $cliente->nombre=$request->get('nombre');
        $cliente->apellido=$request->get('apellido');
        $cliente->num_documento=$request->get('num_documento');
        $cliente->telefono=$request->get('telefono');
        $cliente->direccion=$request->get('direccion');
        $cliente->email=$request->get('email');
        $cliente->estado="Activo";
        $cliente->save();
     Flash::success('El cliente '.$cliente->nombre.' ha sido guardada exitosamente!');
      return Redirect::back();   
    }

  
    public function update(Request $request, $id)
    {
        //
    }

  
    public function destroy($id)
    {
        //
    }
}
