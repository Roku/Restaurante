<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\IngresoFormRequest;
use App\Ingreso;
use App\Detalle_Ingreso;
use DB;

use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use PDF;
use Laracasts\Flash\Flash;



class IngresoController extends Controller
{
  
    public function index(Request $request)
    {
           if($request){
           $ingreso=DB::table('ingreso as i')
           ->join('proveedor as p','i.idproveedor','=','p.idproveedor')
           ->join('detalle_ingreso as di','i.idingreso','=','di.idingreso')
           
           ->join('tipo_comprobante as tipp','i.idtipo_comprobante','=','tipp.idtipo_comprobante')
           ->select('i.idingreso','i.fecha_hora','tipp.nombre','i.num_comprobante','i.estado','i.total_compra','p.empresa','p.nombre as proveedor')
           ->orderBy('i.idingreso','desc')
           ->groupBy('i.idingreso','i.fecha_hora','i.num_comprobante','tipp.nombre','i.estado','i.total_compra','p.empresa')
           ->get();

           return view('compras.ingreso.index',["ingreso"=>$ingreso]);

       }
    }

    
    public function create()
    {
        $proveedor=DB::table('proveedor') ->where('estado','=','Activo')->get();
    
      $tipo_comprobante=DB::table('tipo_comprobante')->where('estado','=','Activo')->get();
       $medida=DB::table('unidad_medida')->where('estado','=','Activo')->get();

      $ingrediente=DB::table('ingrediente as ing')
      ->join('categoria_ingrediente as ci','ci.idcategoria_ingrediente','=','ing.idcategoria_ingrediente')
      ->join('impuesto as imp','imp.idimpuesto','=','ci.idimpuesto')
      ->select('ing.idingrediente','ing.nombre as ingrediente','imp.impuesto','ci.nombre as categoria')
      ->where('ing.estado','=','Activo')
      ->get();

  return view('compras.ingreso.create',["proveedor"=>$proveedor,"ingrediente"=>$ingrediente,"tipo_comprobante"=>$tipo_comprobante,"medida"=>$medida]);


    }

   
    public function store(IngresoFormRequest $request)
    {
         
          DB::beginTransaction();
$ingreso= new Ingreso;
$ingreso->idproveedor=$request->get('idproveedor');    

$ingreso->idtipo_comprobante=$request->get('idtipo_comprobante');
$ingreso->num_comprobante=$request->get('num_comprobante');
$mytime=Carbon::now('America/Guayaquil');
$ingreso->fecha_hora=$mytime->toDateTimeString();
$ingreso->total_compra=$request->get('total_compra');
$ingreso->estado='Activo';
$ingreso->subtotal_compra=$request->get('subtotal_compra');
$ingreso->iva_compra=$request->get('iva_compra');
$ingreso->save();


$idingrediente =$request->get('idingrediente');
$idunidad_medida =$request->get('idunidad_medida');
$cantidad =$request->get('cantidad');
$precio_compra=$request->get('precio_compra');
$precio_venta=$request->get('precio_venta');
//$impuesto=$request->get('impuesto');

$cont=0;

while( $cont < count($idingrediente)) {
    
    $detalle= new Detalle_Ingreso();

$detalle->idingreso=$ingreso->idingreso;
$detalle->idingrediente=$idingrediente[$cont];
$detalle->idunidad_medida=$idunidad_medida[$cont];
$detalle->cantidad= $cantidad[$cont];

$detalle->precio_compra= $precio_compra[$cont];
$detalle->precio_venta=  $precio_venta[$cont];
//$detalle->impuesto=$impuesto[$cont]; 
$detalle->save();
$cont=$cont+1;



}



          DB::commit();

       
      Flash::success('Compra exitosamente!');
        
     return Redirect::to('compras/ingreso');
    }

   
    public function show($id)
    {
        
$ingreso=DB::table('ingreso as i')
          ->join('proveedor as p','i.idproveedor','=','p.idproveedor')
           ->join('detalle_ingreso as di','i.idingreso','=','di.idingreso')
           
           ->join('tipo_comprobante as tipp','i.idtipo_comprobante','=','tipp.idtipo_comprobante')
           ->select('i.idingreso','i.fecha_hora','p.empresa','p.num_documento','p.direccion','p.telefono','tipp.nombre','i.num_comprobante','i.estado','i.total_compra','i.subtotal_compra','i.iva_compra')
           ->where('i.idingreso','=',$id)
           ->first();
           $detalles=DB::table('detalle_ingreso as d')
           ->join('ingreso as i','i.idingreso','=','d.idingreso')
           ->join('ingrediente as ing','ing.idingrediente','d.idingrediente')
           ->join('unidad_medida as udm','udm.idunidad_medida','=','d.idunidad_medida')
           ->select('d.idingreso','ing.nombre as ingrediente','udm.nombre as medida','d.cantidad','d.precio_compra','d.precio_venta')
           ->where('d.idingreso','=',$id)
           ->get();


           return view("compras.ingreso.show",["ingreso"=>$ingreso,"detalles"=>$detalles]);
   


    }

    
    public function edit($id)
    {
        //
    }

  
    public function update(Request $request, $id)
    {
        //
    }

  
    public function destroy($id)
    {
        //
    }
}
