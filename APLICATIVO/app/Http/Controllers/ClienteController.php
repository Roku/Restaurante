<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Cliente;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ClienteFormRequest;
use DB;
use Laracasts\Flash\Flash;

class ClienteController extends Controller
{
       public function index(Request $request)
    {
          if ($request) {
         $cliente=DB::table('cliente')
         ->orderBy('idcliente','desc')
         ->get();

         return view('almacen.cliente.index',["cliente"=>$cliente]);
     }
    }

   
    public function create()
    {
       
    }

   
    public function store(ClienteFormRequest $request)
    {
    	$cliente=new Cliente;
        $cliente->nombre=$request->get('nombre');
        $cliente->apellido=$request->get('apellido');
        $cliente->num_documento=$request->get('num_documento');
        $cliente->telefono=$request->get('telefono');
        $cliente->direccion=$request->get('direccion');
        $cliente->email=$request->get('email');
        $cliente->estado="Activo";
        $cliente->save();
     Flash::success('El cliente '.$cliente->nombre.' ha sido guardada exitosamente!');
        return Redirect::to('almacen/cliente');   
    }

   
    public function show($id)
    {
        //
    }

  
    public function edit($id)
    {
        //
    }

   
    public function update(ClienteFormRequest $request, $id)
    {
       
        $cliente=Cliente::findOrFail($id);    
        $cliente->nombre=$request->get('nombre');
        $cliente->apellido=$request->get('apellido');
        $cliente->num_documento=$request->get('num_documento');
        $cliente->telefono=$request->get('telefono');
        $cliente->direccion=$request->get('direccion');
        $cliente->email=$request->get('email');
        $cliente->update();
     Flash::success('El cliente '.$cliente->nombre.' ha sido actualizado exitosamente!');
        return Redirect::to('almacen/cliente');
    }

   
     public function destroy($id)
    {
        $cliente=Cliente::findOrFail($id);
        $cliente->estado='Inactivo';
        $cliente->update();
              Flash::success('El cliente '.$cliente->nombre.' ha sido eliminado exitosamente!');
         return Redirect::to('almacen/cliente');
    }

    public function restore($id)
    {
       $cliente=Cliente::findOrFail($id);
       $cliente->estado='Activo';
       $cliente->update();
             Flash::success('El cliente '.$cliente->nombre.' ha sido recuperada exitosamente!');
       return Redirect::to('almacen/cliente');
    }
}
