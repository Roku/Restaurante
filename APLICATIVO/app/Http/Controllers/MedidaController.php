<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Medida;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\MedidaFormRequest;
use DB;
use Laracasts\Flash\Flash;

class MedidaController extends Controller
{
   public function __construct($value='')
   {
       # code...
   }
    public function index(Request $request)
    {
        if ($request) {
            $medida=DB::table('unidad_medida')
            ->orderBy('idunidad_medida','desc')
            ->get();

            return view('almacen.medida.index',["medida"=>$medida]);
        }
    }

  
    
    public function store(MedidaFormRequest $request)
    {
        $medida=new Medida;
        $medida->nombre=$request->get('nombre');
        $medida->descripcion=$request->get('descripcion');
        $medida->estado="Activo";
        $medida->save();
        Flash::success('La medida '.$medida->nombre.' ha sido guardada exitosamente!');
        return Redirect::to('almacen/medida');
    }

  
    public function show($id)
    {
        //
    }
 
    public function update(MedidaFormRequest $request, $id)
    {
        $medida=Medida::findOrFail($id);
        $medida->nombre=$request->get('nombre');
        $medida->descripcion=$request->get('descripcion');
        $medida->update();
        Flash::success('La medida '.$medida->nombre.' ha sido actualizada exitosamente!');
        return Redirect::to('almacen/medida');
    }

    
    public function destroy($id)
    {
        $medida=Medida::findOrFail($id);
        $medida->estado="Inactivo";
        $medida->update();
        Flash::error('La medida '.$medida->nombre.' ha sido eliminada exitosamente!');
        return Redirect::to('almacen/medida');
    }
       public function Restore($id)
    {
        $medida=Medida::findOrFail($id);
        $medida->estado="Activo";
        $medida->update();
        Flash::warning('La medida '.$medida->nombre.' ha sido recuperada exitosamente!');
        return Redirect::to('almacen/medida');
    }
}
