<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Dia_especial;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\Dia_EspecialFormRequest;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;
use Laracasts\Flash\Flash;

class Dia_EspecialController extends Controller
{
   
    public function index(Request $request)
    {
        if ($request) {
         $dia_especial=DB::table('dia_especial')
         ->select('nombre','descripcion','imagen','imagendos','imagentres','imagencuatro','estado','iddia_especial')
         ->orderBy('iddia_especial','desc')
         ->get();

         return view('almacen.dia_especial.index',["dia_especial"=>$dia_especial]);
     }
    }

    
    public function create()
    {
        //
    }

  
    public function store(Dia_EspecialFormRequest $request)
    {
        $dia=new Dia_especial;
        $dia->iddia_especial=$request->get('iddia_especial');
        $dia->nombre=$request->get('nombre');
        $dia->descripcion=$request->get('descripcion');
     if (Input::hasFile('imagen')) {
           $file=Input::file('imagen');
           $file->move(public_path().'/imagenes/dia_especial/',$file->getClientOriginalName());
           $dia->imagen=$file->getClientOriginalName();

         
      }
       if (Input::hasFile('imagendos')) {
           $file=Input::file('imagendos');
           $file->move(public_path().'/imagenes/dia_especial/',$file->getClientOriginalName());
           $dia->imagendos=$file->getClientOriginalName();

         
      }
       if (Input::hasFile('imagentres')) {
           $file=Input::file('imagentres');
           $file->move(public_path().'/imagenes/dia_especial/',$file->getClientOriginalName());
           $dia->imagentres=$file->getClientOriginalName();

         
      }
       if (Input::hasFile('imagencuatro')) {
           $file=Input::file('imagencuatro');
           $file->move(public_path().'/imagenes/dia_especial/',$file->getClientOriginalName());
           $dia->imagencuatro=$file->getClientOriginalName();

         
      }

        $dia->estado="Activo";
  $dia->save();
  Flash::success('El día especial '.$dia->nombre.' ha sido guardado  exitosamente!');
  return Redirect::to('almacen/dia');


    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

    
    public function update(Dia_EspecialFormRequest $request, $id)
    {
         $dia=Dia_especial::findOrFail($id);
  $dia->nombre=$request->get('nombre');
  $dia->descripcion=$request->get('descripcion');
     if (Input::hasFile('imagen')) {
           $file=Input::file('imagen');
           $file->move(public_path().'/imagenes/dia_especial/',$file->getClientOriginalName());
           $dia->imagen=$file->getClientOriginalName();

         
      }
           if (Input::hasFile('imagendos')) {
           $file=Input::file('imagendos');
           $file->move(public_path().'/imagenes/dia_especial/',$file->getClientOriginalName());
           $dia->imagendos=$file->getClientOriginalName();

         
      }
       if (Input::hasFile('imagentres')) {
           $file=Input::file('imagentres');
           $file->move(public_path().'/imagenes/dia_especial/',$file->getClientOriginalName());
           $dia->imagentres=$file->getClientOriginalName();

         
      }
       if (Input::hasFile('imagencuatro')) {
           $file=Input::file('imagencuatro');
           $file->move(public_path().'/imagenes/dia_especial/',$file->getClientOriginalName());
           $dia->imagencuatro=$file->getClientOriginalName();

         
      }
    
  $dia->update();
        Flash::success('El día especial '.$dia->nombre.' ha sido actualizado exitosamente!');
  return Redirect::to('almacen/dia');
    }

    
   public function destroy($id)
    {
        $dia=Dia_especial::findOrFail($id);
        $dia->estado='Inactivo';
        $dia->update();
        Flash::error('El día especial '.$dia->nombre.' ha sido eliminado exitosamente!');
         return Redirect::to('almacen/dia');
    }
     public function restore($id)
    {
       $dia=Dia_especial::findOrFail($id);
       $dia->estado='Activo';
       $dia->update();
       Flash::warning('El día '.$dia->nombre.' ha sido recuperado exitosamente!');
       return Redirect::to('almacen/dia');
    }
}
