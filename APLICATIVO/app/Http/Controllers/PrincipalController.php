<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PrincipalController extends Controller

{
    public function app(Request $request)
    {
  //    SELECT m.nombre,ingr.nombre,idm.nombre,dtr.cantidad,pre.preparacion FROM detalle_receta as dtr
        // INNER JOIN menu as m ON(dtr.idmenu = m.idmenu)
        // INNER JOIN ingrediente as ingr ON(dtr.idingrediente=ingr.idingrediente)
        // INNER JOIN unidad_medida as idm ON(dtr.idunidad_medida=idm.idunidad_medida)
        // INNER JOIN preparacion as pre ON(m.idmenu=pre.idmenu)
        // WHERE m.estado='Inactivo'
        if ($request) {
            $detalle_receta=DB::table('detalle_receta as dtr')
            ->join('menu as m','m.idmenu','=','dtr.idreceta')
            ->join('ingrediente as ingr','dtr.idingrediente','=','ingr.idingrediente')
            ->join('unidad_medida as idm','dtr.idunidad_medida','=','idm.idunidad_medida')
            ->join('preparacion as pre','m.idmenu','=','pre.idmenu')
            ->select('m.nombre as nombremenu','m.idmenu as idmenu','ingr.nombre as ningrediente','idm.nombre','dtr.cantidad','pre.preparacion')
            ->where('m.estado','=','Activo')
            ->get();

            $categoria=DB::table('categoria as c')
            ->select('idcategoria as categoria','nombre','descripcion')
            ->where('estado','=','Activo')
            ->get();

            $menu=DB::table('menu as m')
            ->select('nombre','idmenu','idcategoria as categoria','imagen')
            ->where('estado','=','Activo')
            ->get();

            $diaespecial=DB::table('dia_especial')
            ->select('iddia_especial','nombre','descripcion','imagen','imagendos','imagentres','imagencuatro','estado')
            ->get();
            return view('PAGEWEB.app',["detalle_receta"=>$detalle_receta,"menu"=>$menu,"categoria"=>$categoria,"diaespecial"=>$diaespecial]);
        }
     
    }
}
