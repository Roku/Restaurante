<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Impuesto;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ImpuestoFormRequest;
use DB;
use Laracasts\Flash\Flash;


class ImpuestoController extends Controller
{
    
    public function index(Request $request)
    {
            if ($request) {
         $impuesto=DB::table('impuesto')
         ->orderBy('idimpuesto','desc')
         ->get();

         return view('almacen.impuesto.index',["impuesto"=>$impuesto]);
     }

    }

   

    public function store(Request $request)
    {
        
       $impuesto=new Impuesto;
        $impuesto->impuesto=$request->get('impuesto');
        $impuesto->descripcion=$request->get('descripcion');
        $impuesto->estado="Activo";
        $impuesto->save();
     Flash::success('El impuesto '.$impuesto->impuesto.' ha sido guardada exitosamente!');
        return Redirect::to('almacen/impuesto');   
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
       $impuesto=Impuesto::findOrFail($id);
        $impuesto->impuesto=$request->get('impuesto');
        $impuesto->descripcion=$request->get('descripcion');
        $impuesto->update();
      Flash::success('El impuesto ha sido actualizada exitosamente!');
     return Redirect::to('almacen/impuesto');
    }

   
     public function destroy($id)
    {
        $impuesto=Impuesto::findOrFail($id);
        $impuesto->estado='Inactivo';
        $impuesto->update();
              Flash::success('El impuesto ha sido eliminada exitosamente!');
         return Redirect::to('almacen/impuesto');
    }

    public function restore($id)
    {
       $impuesto=Impuesto::findOrFail($id);
       $impuesto->estado='Activo';
       $impuesto->update();
             Flash::success('El impuesto ha sido recuperada exitosamente!');
       return Redirect::to('almacen/impuesto');
    }
}
