<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle_Receta extends Model
{
  protected $table='detalle_receta';
    protected $primaryKey='iddetalle_receta';
    public $timestamps=false;

    protected $fillable=[
'idingrediente',
'idunidad_medida',
'idreceta',
'cantidad'

    ];
    protected $guarded=[
    ];
}
