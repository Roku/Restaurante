<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
   protected $table='ingreso';
    protected $primaryKey='idingreso';
    public $timestamps=false;

    protected $fillable=[
'idproveedor',
'idtipo_comprobante',
'num_comprobante',
'fecha_hora',
'subtotal_compra',
'iva_compra',
'total_compra',
'estado'

    ];
    protected $guarded=[
    ];
}
