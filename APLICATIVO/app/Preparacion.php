<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preparacion extends Model
{
    protected $table='preparacion';
    protected $primaryKey='idpreparacion';
    public $timestamps=false;

    protected $fillable=[
'idmenu',
'preparacion',
'descripcion'

    ];

    protected $guarded=[

    ];

}
