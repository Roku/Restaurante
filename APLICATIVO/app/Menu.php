<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table='menu';
    protected $primaryKey='idmenu';
    public $timestamps=false;

    protected $fillable=[
'idcategoria',
'nombre',
'precio_venta',
'descripcion',
'fecha',
'imagen',
'estado'

    ];

    protected $guarded=[
    	
    ];
}
