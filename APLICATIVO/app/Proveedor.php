<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table='proveedor';
    protected $primaryKey='idproveedor';
    public $timestamps=false;

    protected $fillable=[
		'empresa',
		'nombre',
		'apellido',
		'num_documento',
		'telefono',
		'direccion',
		'email',
		'estado',


    ];

    protected $guarded=[


    ];
}
