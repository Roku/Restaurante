<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_Comprobante extends Model
{
  protected $table='tipo_comprobante';
    protected $primaryKey='idtipo_comprobante';
    public $timestamps=false;

    protected $fillable=[
		'nombre',
		'descripcion',
		'estado'

    ];

    protected $guarded=[


    ];
}
