<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Impuesto extends Model
{
     protected $table='impuesto';
    protected $primaryKey='idimpuesto';
    public $timestamps=false;

    protected $fillable=[
		'impuesto',
		'descripcion',
		'estado'

    ];

    protected $guarded=[


    ];
}
