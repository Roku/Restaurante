<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria_Ingrediente extends Model
{
     protected $table='categoria_ingrediente';
    protected $primaryKey='idcategoria_ingrediente';
    public $timestamps=false;

    protected $fillable=[
        'idcategoria_ingrediente',
        'idimpuesto',
		'nombre',
		'descripcion',
        'estado '

    ];

    protected $guarded=[


    ];
}
