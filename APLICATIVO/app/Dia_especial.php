<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dia_especial extends Model
{
    protected $table='dia_especial';
    protected $primaryKey='iddia_especial';
    public $timestamps=false;

    protected $fillable=[
'nombre',
'descripcion',
'imagen',
'imagendos',
'imagentres',
'imagencuatro',
'estado'
    ];
    protected $guarded=[
    ];
}
