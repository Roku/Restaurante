<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle_Ingreso extends Model
{
   protected $table='detalle_ingreso';
    protected $primaryKey='iddetalle_ingreso';
    public $timestamps=false;

    protected $fillable=[
'idingreso',
'idingrediente',
'idunidad_medida',
'cantidad',
'precio_compra',
'precio_venta'

    ];
    protected $guarded=[
    ];
}
