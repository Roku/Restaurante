<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
     protected $table='receta';
    protected $primaryKey='idreceta';
    public $timestamps=false;

    protected $fillable=[
'idmenu',
'coccion',
'peso_receta',
'peso_porcion',
'porciones',
'estado'

    ];

    protected $guarded=[

    ];
}
