<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class venta extends Model
{
   protected $table='venta';
    protected $primaryKey='idventa';
    public $timestamps=false;

    protected $fillable=[
'idcliente',
'idtipo_comprobante',
'num_comprobante',
'fecha_hora',
'subtotal_venta',
'iva_venta',
'total_venta',
'estado'

    ];
    protected $guarded=[
    ];
}
