<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredientes extends Model
{
    protected $table='ingrediente';
    protected $primaryKey='idingrediente';
    public $timestamps=false;
    protected $fillable=[
        'idcategoria_ingrediente',
    	'nombre',
    	'descripcion',
    	'estado'
    ];
    public static function ingred($id)
    {
        return Ingredientes::where('idcategoria_ingrediente','=',$id)
        ->get();
    }
    protected $guarded=[

    ];
     public static function categoria_ingrediente($id)
    {
        return Ingredientes::where('idcategoria_ingrediente', '=', $id)->get();
    }
}
