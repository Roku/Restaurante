<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle_Venta extends Model
{
  protected $table='detalle_venta';
    protected $primaryKey='iddetalle_venta';
    public $timestamps=false;

    protected $fillable=[
'idventa',
'idmenu',
'idunidad_medida',
'cantidad',
'precio_venta'

    ];
    protected $guarded=[
    ];
}
