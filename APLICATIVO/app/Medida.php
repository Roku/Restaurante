<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medida extends Model
{
    protected $table='unidad_medida';
    protected $primaryKey='idunidad_medida';
    public $timestamps=false;

    protected $fillable=[
'nombre',
'descripcion',
'estado'
    ];
    protected $guarded=[
    ];
}
