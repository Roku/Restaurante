 $(document).ready(function(){
$('#tabla').DataTable({

 "language":fecha,
 "lengthMenu": [[6, 10, 25, -1], [6, 10, 25, "Todos"]]
  

 });

});


 var fecha={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar Listado en _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "<big><strong>TOTAL REGISTROS _TOTAL_ </big></strong> ",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "<big><strong>(DE UN TOTAL _MAX_ REGISTROS REALIZADOS)</big></strong>",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}


   


