@extends ('layouts.admin')
@section ('contenido')
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{
font-family:Arial, sans-serif;
font-size:14px;
padding:10px 5px;
border-width:1px;
overflow:hidden;
word-break:normal;
}
.tg th{font-family:Arial, sans-serif;
font-size:14px;
font-weight:normal;
padding:10px 5px;
border-width:1px;
overflow:hidden;
word-break:normal;
}
.tg .tg-yw4l{vertical-align:top}
table{
box-shadow: 1px 3px 4px rgba(0,0,0,0.4);
}
</style>
{!!Form::open(array('url'=>'almacen/receta','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}}
<h3  style="text-align:center;color:#2C4E79; font-weight: bold;">DETALLE DE LA RECETA RECETA </h3>
<table class="tg table table-responsive" style="padding:15px;width: 95%;margin: auto;border-radius: 10px">
  <colgroup>
  <col style="width: 259px">
  <col style="width: 269px">
  <col style="width: 298px">
  </colgroup>
  <tr>
    <th class="tg-031e" colspan="2" style="padding-left: 5%;"  ><strong>NOMBRE DE LA RECETA: </strong></th>
    <th class="tg-yw4l" style="padding-left: 5%;" ><strong> TÉCNICAS DE COCCIÓN :</strong></th>
  </tr>
  <tr>
    <td class="tg-031e" colspan="2" style="padding-left: 5%;" >{{ $menu->menu }}</td>
    <td class="tg-yw4l" style="padding-left: 5%;" ><input type=" " style="width: 100%; border: none;" name="coccion" id="pcoccion" value="" class="form-control"></td>
  </tr>
  <tr>
    <td class="tg-yw4l" style="padding-left: 5%;" ><strong> PESO DE LA RECETA :</strong></td>
    <td class="tg-yw4l" style="padding-left: 5%;" ><strong>PESO POR PORCIÓN: </strong></td>
    <td class="tg-yw4l" style="padding-left: 5%;" ><strong> N DE PORCIONES:</strong></td>
  </tr>
  <tr>
    <td class="tg-yw4l" style="padding-left: 5%;" ><input type=" " class="form-control" style="width: 100%; border: none;" name="peso_receta" id="ppeso_receta" value="" style="width: 100%; border: none;" ></td>
    <td class="tg-yw4l" style="padding-left: 5%;" ><input type=" " class="form-control" style="width: 100%; border: none;" name="peso_porcion" id="ppeso_porcion" value="" ></td>
    <td class="tg-yw4l" style="padding-left: 5%;" ><input type=" " class="form-control" style="width: 100%; border: none;" name="porciones" id="pporcion" value="" ></td>
  </tr>
</table>
<input type=" " name="idmenu" id="pmenu" value="{{ $menu->idmenu }}" style="display: none;">

<div class="container">
  <div class="row">
        <div class="col-sm-11">
      <h3  style="text-align:center;color:#2C4E79; font-weight: bold;">AGREGAR INGREDIENTES A LA RECETA RECETA </h3>
    </div>

    <div class="panel panel-body-primary  col-sm-11">
      <div class="panel-body col-sm-12">

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-11">
          <div class="form-group">
            <label >Categoría:</label>           
            {!! Form::select('categorias',$categ,null,['placeholder'=>'SELECCIONE','id'=>'catingrediente','class' => 'form-control selectpicker']) !!}
            
          </div>
        </div>
        <div class="col-lg-4 col-sm-4 col-md-4">
          <div class="form-group">
            <label >Ingrediente:</label>
            {!! Form::select('ingredientes',['placeholder'=>'SELECT'],null,['id'=>'pidingrediente','class'=>'form-control'])!!}
          </div>
        </div>
        
        
        <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2">
          <div class="form-group">
            <label for="cantidad">Cantidad</label>
            <input type="number" name="pcantidad" id="pcantidad" class="form-control" placeholder="0.00" >
          </div>
          
        </div>
        <div class="col-lg-2 col-sm-6 col-md-6 col-xs-11">
          <div class="form-group">
            <label >Medida:</label>
            <select name="pidunidad_medida" id="pidunidad_medida" class="form-control selectpicker" data-live-search="true">
              @foreach($medida as $med)
              <option value="{{$med->idunidad_medida}}">{{$med->nombre}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-lg-2 col-sm-2 col-md-2 col-xs-11">
          
          <div class="form-group">
            <br>
            <button type="button" id="bt_add" class="btn btn-success" style="width: 140px; height: 39px;">Agregar</button>
          </div>
        </div>
        <div>
          <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
            <thead style="background-color:#A9D0F5 ">
              <tr>
                <th>Opciones</th>
                <th>Unidad Métrico</th>
                <th>Medida</th>
                <th>Ingredientes</th>
              </tr>
            </thead>
            
          </table>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-11" id="guardar">
      <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button class="btn btn-primary" type="submit">Guardar</button>
        
      </div>
    </div>
  </div>
</div>
{!!Form::close()!!}
@push('scripts')

<script>
$(document).ready(function(){
$('#bt_add').click(function(){
agregar();
});



});



var cont=0;
function agregar() {
idingrediente=$("#pidingrediente").val();
ingrediente=$("#pidingrediente option:selected").text();
cantidad=$("#pcantidad").val();
idunidad_medida=$("#pidunidad_medida").val();
unidad_medida=$("#pidunidad_medida option:selected").text();
idmenu=$("#pmenu").val();
if (idingrediente!="" && cantidad!="") {
var fila='<tr class="select" id="fila'+cont+'"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td><td><input type="number" name="cantidad[]" value="'+cantidad+'" readonly style="border:none"></td><td><input type="hidden" name="idunidad_medida[]" value="'+idunidad_medida+'">'+unidad_medida+'</td><td><input type="hidden" name="idingrediente[]" value="'+idingrediente+'">'+ingrediente+'</td></tr>';
cont++;
limpiar();
$('#detalles').append(fila);
}
}
function limpiar(){
$("#pcantidad").val("");
}
function eliminar(index) {
$("#fila" + index).remove();
}




</script>
@endpush
@endsection