@extends('layouts.admin')
@section('contenido')

@include('flash::message')


<div class="content">
  <div class="row" >
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <br>
    <h3>&nbsp&nbsp&nbspLISTADO DE RECETAS. <a href="{{ URL('receta/detalle') }}"><button class="btn btn-success"><i class="fa fa-pencil"> NUEVO </i></button></a></h3>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="tabla-responsive">
      <table id="tabla" class="table table-hover">
        <thead class="fondo">
          <tr>
            <th>RECETA DEL MENÚ</th>
            <th>TIPO DE COCCIÓN</th>
            <th>PESO RECETA</th>
            <th>PESO PORCIÓN</th>
             <th>PORCIONES</th>
             <th>ESTADO</th>
              <th>OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($receta as $men)
          <tr>
            <td>{{$men->nombre}}</td>
            <td>{{$men->coccion}}</td>
             <td>{{$men->peso_receta}}</td>
            <td>{{$men->peso_porcion}}</td>
             <td>{{$men->porciones}}</td>
            @if($men->estado=="Activo")
            <td> <span class="label label-success">Activo</span></td>
            <td>
                <a href="{{URL::action('Detalle_RecetaController@show',$men->idmenu)}}" ><button class="btn btn-primary" title="VER MAS" ><i class=" fa fa-eye"></i></button></a>
                       <a href="{{URL::action('Detalle_RecetaController@edit',$men->idmenu)}}" ><button class="btn btn-info" title="DEVOLUCIÓN" ><i class=" fa fa-edit"></i></button></a>
              <a href="" data-target="#modal-Delete-{{$men->idmenu}}" data-toggle="modal" ><button class="btn btn-danger" title="ELIMINAR" ><i class="fa fa-trash"></i></button></a>
            </td>
            @else
            <td><span class="label label-danger">Inactivo</span></td>
            <td>
              <a href="" data-target="#modal-Restore-{{$men->idmenu}}" data-toggle="modal"><button class="btn btn-warning" >RECUPERAR</button></a>
            </td>
            @endif
          </tr>
           
          @endforeach
        </tbody>
        
      </table>
    </div>
  </div>
</div>
</div>
@endsection