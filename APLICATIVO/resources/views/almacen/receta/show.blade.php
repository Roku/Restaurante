@extends ('layouts.admin')
@section ('contenido')
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{
font-family:Arial, sans-serif;
font-size:14px;
padding:10px 5px;
border-width:1px;
overflow:hidden;
word-break:normal;
}
.tg th{
font-family:Arial, sans-serif;
font-size:14px;
font-weight:normal;
padding:10px 5px;
border-width:1px;
overflow:hidden;
word-break:normal;
}
.tg .tg-yw4l{vertical-align:top}
table{
box-shadow: 1px 3px 4px rgba(0,0,0,0.4);
}
</style>
<h3  style="text-align:center;color:#2C4E79; font-weight: bold;">DETALLE DE LA RECETA RECETA </h3>
<table class="tg table table-responsive" style="padding:15px;width: 95%;margin: auto;border-radius: 10px">
  <colgroup>
  <col style="width: 259px">
  <col style="width: 269px">
  <col style="width: 298px">
  </colgroup>
  <tr>
    <th class="tg-031e" colspan="2"  style="padding-left: 5%;"><strong>NOMBRE DE LA RECETA: </strong></th>
    <th class="tg-yw4l"  style="padding-left: 5%;"><strong> TÉCNICAS DE COCCIÓN :</strong></th>
  </tr>
  <tr>
    <td class="tg-031e" colspan="2"  style="padding-left: 5%;">{{ $receta->nombre }}</td>
    <td class="tg-yw4l"  style="padding-left: 5%;"><input class="form-control" type=" " name="coccion" id="pcoccion" value="{{$receta->coccion}}" style="width: 300px; border: none;"></td>
  </tr>
  <tr>
    <td class="tg-yw4l"  style="padding-left: 5%;"><strong> PESO DE LA RECETA :</strong></td>
    <td class="tg-yw4l"  style="padding-left: 5%;"><strong>PESO POR PORCIÓN: </strong></td>
    <td class="tg-yw4l"  style="padding-left: 5%;"><strong> N DE PORCIONES:</strong></td>
  </tr>
  <tr>
    <td class="tg-yw4l"  style="padding-left: 5%;"><input type=" " class="form-control" name="peso_receta" id="ppeso_receta" value="{{$receta->peso_receta}}" style="width: 300px; border: none;"></td>
    <td class="tg-yw4l"  style="padding-left: 5%;"><input type=" " class="form-control" name="peso_porcion" id="ppeso_porcion" value="{{$receta->peso_porcion}}" style="width: 300px; border: none;"></td>
    <td class="tg-yw4l"  style="padding-left: 5%;"><input type=" " class="form-control" name="porciones" id="pporcion" value="{{$receta->porciones}}" style="width: 300px; border: none;"></td>
  </tr>
</table>
<br>
<div class="container">
  <div class="row">
    <div class="form-group col-sm-2">
      <input type="text" style="width: 100%"  name="nuevareceta" id="nuevareceta" value="" class="form-control">
      <!--aqui ingreso el valor por el que necesito multiplicar -->
      <button type="button" style="width: 100%" name="calcular" class="btn btn-info" id="calcular">calcular</button>
    </div>
    <div class="col-sm-2">
      <table class="table table-bordered table-condensed" style="width: 100%;">
        <thead style="background-color:#A9D0F5">
          <tr>
            <th style="padding: 5px;">NUEVAS MEDIDAS</th>
          </tr>
        </thead>
        <tbody id="totaldeber">
          
        </tbody>
      </table>
    </div>
    <div class="col-sm-6">
      <table id="detalles" style="width: 100%;" class="table  table-bordered table-condensed">
        <thead style="background-color:#A9D0F5 ">
          <tr class="idgeneral">
            <th>Unidad Métrico</th>
            <th>Medida</th>
            <th>Ingredientes</th>
          </tr>
        </thead>
        <tbody id="prueba">
          @foreach($detalles as $dt)
          <tr>
            <td class="numero" data-cantidad="{{ $dt->cantidad }}">{{ $dt->cantidad}}</td>
            <td>{{ $dt->medida  }}</td>
            <td>{{ $dt->nombre }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@push('scripts')
<script>
$(document).ready(function() {
$("#calcular").on('click', function(e) {
$('#totaldeber').empty();

var recetaestandar = $('#pporcion').val();
var recetanueva = $('#nuevareceta').val();
var formula = (recetanueva/recetaestandar);
console.log('event: #calcular:click');
var valor_final = 0;
var numeros = $("#detalles").find('.numero');
console.log(numeros);
for(var i = 0; i < numeros.length; i++)
{
var numero = parseFloat($(numeros[i]).data('cantidad'));
$('#totaldeber').append('<tr><td>' + (numero * formula).toFixed(2) + '</td></tr>');
}
});
});
</script>
@endpush
@endsection