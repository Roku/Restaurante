@extends('layouts.admin')
@section('contenido')
<div class="row">
  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <br>
    <h3>&nbsp&nbsp&nbspLISTADO DE MENUS PARA RECETAS. </h3>
  </div>
</div>
<div class="row" >
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="tabla-responsive">
      <table id="tabla" class="table table-hover">
        <thead class="fondo">
          <tr>
            <th>MENÚ</th>
            <th>CATEGORÍA</th>
            <th>ESTADO</th>
            <th>OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($menu as $men)
          <tr>
            <td>{{$men->menu}}</td>
            <td>{{$men->categoria}}</td>
            @if($men->estado=="Activo")
            <td> <span class="label label-success">Activo</span></td>
            <td>
              <a href="{{URL::action('Detalle_RecetaController@create',$men->idmenu)}}" ><button class="btn btn-primary" title="DEVOLUCIÓN" ><i class=" fa fa-mail-reply-all"></i></button></a>
             
            
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection