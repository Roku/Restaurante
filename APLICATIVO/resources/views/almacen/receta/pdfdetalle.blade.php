<h3  style="text-align:center;color:#2C4E79; font-weight: bold;">RECETA PARA {{$menu->menu}}</h3>
<br>
<br>
<div class="row">
  <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
  </div>
  <div>
    <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
      <thead style="background-color:#A9D0F5 ">
        <tr>
          
          <th>Ingrediente</th>
          <th>Cantidad</th>
          <th>Medida</th>
        </tr>
      </thead>
      <tbody>
        @foreach($detalles as $dt)
        <tr>
          <td>{{ $dt->nombre }}</td>
          <td>{{ $dt->cantidad }}</td>
          <td>{{ $dt->medida  }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  
</div>