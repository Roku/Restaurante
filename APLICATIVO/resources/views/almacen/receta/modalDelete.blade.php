
<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-Delete-{{$dt->iddetalle_receta}}">
	{{Form::Open(array('action'=>array('Detalle_RecetaController@eliminardetalle',$dt->iddetalle_receta),'method'=>'GET'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #d9534f;">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">ELIMINAR DETALLE DE LA RECETA</font></h3>
			</div>
			<div class="modal-body">
				<p><font size="4">Confirme si desea Eliminar ingrediente de la receta</font></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" onclick="recargar()" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>