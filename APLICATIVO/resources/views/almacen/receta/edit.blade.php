@extends ('layouts.admin')
@section ('contenido')
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{
font-family:Arial, sans-serif;
font-size:14px;
padding:10px 5px;
border-width:1px;
overflow:hidden;
word-break:normal;
}
.tg th{font-family:Arial, sans-serif;
font-size:14px;
font-weight:normal;
padding:10px 5px;
border-width:1px;
overflow:hidden;
word-break:normal;
}
.tg .tg-yw4l{vertical-align:top}
table{
box-shadow: 1px 3px 4px rgba(0,0,0,0.4);
}
</style>
{{ Form::model($receta,['method'=>'PATCH','action'=>['Detalle_RecetaController@update',$receta->idreceta],'autocomplete'=>'off']) }}
{{ form::token() }}
<h3  style="text-align:center;color:#2C4E79; font-weight: bold;">EDITAR EL DETALLE DE LA RECETA RECETA </h3>
<table class="tg table table-responsive" style="padding:15px;width: 95%;margin: auto;border-radius: 10px">
  <tr>
    <th class="tg-031e" style="padding-left: 5%;" colspan="2"><strong>NOMBRE DE LA RECETA: </strong></th>
    <th class="tg-yw4l"><strong> TÉCNICAS DE COCCIÓN :</strong></th>
  </tr>
  <tr>
    <td class="tg-031e" style="padding-left: 5%;" colspan="2">{{ $receta->nombre }}</td>
    <td class="tg-yw4l"><input type=" " name="coccion" class="form-control" id="pcoccion" value="{{$receta->coccion}}" style="width: 100%; border: none;"></td>
  </tr>
  <tr>
    <td class="tg-yw4l" style="padding-left: 5%;"><strong> PESO DE LA RECETA :</strong></td>
    <td class="tg-yw4l"><strong>PESO POR PORCIÓN: </strong></td>
    <td class="tg-yw4l"><strong> N DE PORCIONES:</strong></td>
  </tr>
  <tr>
    <td class="tg-yw4l" style="padding-left: 5%;"><input type=" " class="form-control name="peso_receta" id="ppeso_receta" value="{{$receta->peso_receta}}" style="width: 100%; border: none;"></td>
    <td class="tg-yw4l"><input type=" " class="form-control name="peso_porcion" id="ppeso_porcion" value="{{$receta->peso_porcion}}" style="width: 100%; border: none;"></td>
    <td class="tg-yw4l"><input type=" " class="form-control name="porciones" id="pporcion" value="{{$receta->porciones}}" style="width: 100%; border: none;"></td>
  </tr>
</table>
<br>
<div class="container col-sm-12">
  <div class="row">
    <div class="container col-sm-12">
      <div class="col-lg-3 col-sm-3 col-md-3">
        <div class="form-group">
          <label >Ingrediente:</label>
          
          <select name="pidingrediente" id="pidingrediente" class="form-control selectpicker" data-live-search="true">
            @foreach($ingrediente as $ing)
            <option value="{{$ing->idingrediente}}">{{$ing->nombre}}</option>
            @endforeach
          </select>
        </div>
      </div>
      
      
      <div class="col-lg-1 col-sm-1 col-md-1 col-xs-12">
        <div class="form-group">
          <label for="cantidad">Cantidad</label>
          <input type="number" name="pcantidad" id="pcantidad" class="form-control" placeholder="0" >
        </div>
        
      </div>
      <div class="col-lg-1 col-sm-6 col-md-6 col-xs-12">
        <div class="form-group">
          <label >Medida:</label>
          <select name="pidunidad_medida" id="pidunidad_medida" class="form-control selectpicker" data-live-search="true">
            @foreach($medida as $med)
            <option value="{{$med->idunidad_medida}}">{{$med->nombre}}</option>
            @endforeach
          </select>
        </div>
      </div><br>
      <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
          <button type="button" id="bt_add" class="btn btn-success" style="width: 80%; height: 39px;">Agregar</button>
      
      </div>
      <div class="col-lg-2 col-sm-3 col-md-3 col-xs-12" id="guardar">

          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button class="btn btn-primary" type="submit" style="width: 80%; height: 39px;">Guardar Receta</button>

      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
      
      <div>
        <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
          <thead style="background-color:#A9D0F5 ">
            <tr>
              <th>Opciones</th>
              <th>Unidad Métrico</th>
              <th>Medida</th>
              <th>Ingredientes</th>
              
            </tr>
          </thead>
          <tbody>
            @foreach($detalles as $dt)
            <tr>
              <td><a href="" data-target="#modal-Delete-{{$dt->iddetalle_receta}}" data-toggle="modal" ><button class="btn btn-danger" title="ELIMINAR" ><i class="fa fa-trash"></i></button></a></td>
              <td>{{ $dt->cantidad }}</td>
              <td>{{ $dt->medida  }}</td>
              <td>{{ $dt->nombre }}</td>
            </tr>
            @include('almacen.receta.modalDelete')
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
{!!Form::close()!!}
@push('scripts')
<script>
$(document).ready(function(){
$('#bt_add').click(function(){
agregar();
});
});
var cont=0;
function agregar() {
idingrediente=$("#pidingrediente").val();
ingrediente=$("#pidingrediente option:selected").text();
cantidad=$("#pcantidad").val();
idunidad_medida=$("#pidunidad_medida").val();
unidad_medida=$("#pidunidad_medida option:selected").text();
idmenu=$("#pmenu").val();
if (idingrediente!="" && cantidad!="") {
var fila='<tr class="select" id="fila'+cont+'"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td><td><input type="number" name="cantidad[]" value="'+cantidad+'" readonly style="border:none"></td><td><input type="hidden" name="idunidad_medida[]" value="'+idunidad_medida+'">'+unidad_medida+'</td><td><input type="hidden" name="idingrediente[]" value="'+idingrediente+'">'+ingrediente+'</td></tr>';
cont++;
limpiar();
$('#detalles').append(fila);
}
}
function limpiar(){
$("#pcantidad").val("");
}
function eliminar(index) {
$("#fila" + index).remove();
}
</script>
@endpush
@endsection