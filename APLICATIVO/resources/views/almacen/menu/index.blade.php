@extends('layouts.admin')
@section('contenido')

@include('almacen.menu.modalCreate')
@include('flash::message')
<div class="content">
  <div class="row" >
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <br>
    <h3>&nbsp&nbsp&nbspLISTADO DE MENÚ. <a href="" data-target="#modal-Create" data-toggle="modal" ><button class="btn btn-primary" >NUEVO</button></a> </h3>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="tabla-responsive">
      <table id="tabla" class="table table-hover">
        <thead class="fondo">
          <tr>
            
            <th>MENÚ</th>
            <th>CATEGORÍA</th>
            <th>PRECIO</th>
            <th>FECHA</th>
            <th>IMAGEN</th>
            <th>ESTADO</th>
            <th>OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($menu as $men)
          <tr>
            <td>{{$men->nombre}}</td>
             <td>{{$men->categoria}}</td>
            <td>{{$men->precio_venta}}</td>
            <td>{{$men->fecha}}</td>
             <td>
           <img src="{{asset('imagenes/menu/'.$men->imagen)}}" alt="{{$men->nombre}}" height="10px" width="80px" class="img-thumbnail">

        </td>
            @if($men->estado=="Activo")
            <td> <span class="label label-success">Activo</span></td>
            <td>
              <a href="" data-target="#modal-Edit-{{$men->idmenu}}" data-toggle="modal" ><button class="btn btn-info" title="EDITAR" ><i class="fa fa-edit"></i></button></a>
              <a href="" data-target="#modal-Delete-{{$men->idmenu}}" data-toggle="modal" ><button class="btn btn-danger" title="ELIMINAR" ><i class="fa fa-trash"></i></button></a>
            </td>
            @else
            <td><span class="label label-danger">Inactivo</span></td>
            <td >
               <a href="" data-target="#modal-Restore-{{$men->idmenu}}" data-toggle="modal"><button class="btn btn-warning" >RECUPERAR</button></a>
            </td>
            @endif
          </tr>
        @include('almacen.menu.modalEdit')
        @include('almacen.menu.modalDelete')
        @include('almacen.menu.modalRestore')
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
<br>
</div>
@endsection