<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" id="modal-Create">
	{{Form::Open(array('action'=>array('MenuController@store'),'method'=>'POST','files'=>'true'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #356c8c;">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">CREAR MENÚ</font></h3>
			</div>
			<div class="modal-body">
				
{!!Form::token()!!}
    <div class="form-group">
    <label for="nombre">Menú: </label>
    <input type="text" name="nombre" class="form-control" placeholder="Menú..." value="{{ old('nombre') }}">	
    </div>
     <div class="form-group">
     <label>Categoría</label>
   
        <select name="idcategoria"  class="form-control selectpicker" data-live-search="true">
        @foreach($categoria as $cat)
              <option value="{{ $cat->idcategoria }}">{{ $cat->nombre }}</option>
      @endforeach
          </select>  
    </div>
  <div class="form-group">
    <label for="descripcion">Precio Venta: </label>
    <input type="number" name="precio_venta" class="form-control" placeholder="Precio de venta..." value="" >    
    </div>
     <div class="form-group" style="display: none;">
    <label for="descripcion">Descripción: </label>
    <input type="text" name="descripcion" class="form-control" placeholder="Descripcion..." value="{{ old('descripcion')}}">	
    </div>
  
     <div class="form-group">
        <label for="imagen">Imagen del menú:</label>
        <input type="file" name="imagen"  class="form-control">
      </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
