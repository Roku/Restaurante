<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" id="modal-Edit-{{$men->idmenu}}">
    {{Form::Open(array('action'=>array('MenuController@update',$men->idmenu),'method'=>'PATCH','files'=>'true'))}}
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background: #356c8c;">
                <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">EDITAR MENÚ</font></h3>
            </div>
            <div class="modal-body">
                {{Form::token()}}
                <input type="" name="idmenu" value="{{ $men->idmenu }}" style="display: none">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="nombre">Menú: </label>
                            <input type="text" name="nombre" class="form-control" value="{{$men->nombre}}" placeholder="Menú..." >
                        </div>
                        <div class="form-group">
                            <label>Categoria</label>
                            
                            <select class=" form-control " data-live-search="true" name="idcategoria" >
                                @foreach($categoria as $cat)
                                @if($cat->idcategoria==$men->idcategoria)
                                <option value="{{ $cat->idcategoria }}" selected >{{ $cat->nombre }}</option>
                                @else
                                <option value="{{ $cat->idcategoria }}">{{ $cat->nombre }}</option>
                                
                                @endif
                                @endforeach
                                
                            </select>
                            
                        </div>
                          <div class="form-group">
    <label for="descripcion">Precio Venta: </label>
    <input type="number" name="precio_venta" class="form-control" placeholder="Precio de venta..." value="{{ $men->precio_venta }}" >    
    </div>
                        <div class="form-group" style="display: none;">
                            <label for="descripcion">Descripción: </label>
                            <input type="text" name="descripcion" class="form-control" value="{{$men->descripcion}}" placeholder="Descripcion..." style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">
                        </div>
                        
                        <div class="form-group">
                            <label for="imagen">Imagen del menú:</label>
                            <input type="file" name="imagen"  class="form-control">
                            
                        </div>
                    </div>
                    <div class="col-sm-4">
                        
                            @if(($men->imagen)!="")
                            <img src="{{asset('imagenes/menu/'.$men->imagen)}}"  height="300px" width="400px" style="border-radius: 8px; box-shadow: 2px 4px 6px rgba(0,0,0,0.5)">
                            @endif
                    </div>

                </div>


                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Confirmar</button>
            </div>
        </div>
    </div>
    {{Form::Close()}}
</div>