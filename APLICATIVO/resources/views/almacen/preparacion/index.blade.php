@extends('layouts.admin')
@section('contenido')
@include('flash::message')
<div class="content">
  <div class="row" >
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <br>
    <h3>&nbsp&nbsp&nbspLISTADO DE PREPARACIÓN. <a href="{{ URL('preparacion/detalle') }}"><button class="btn btn-success"><i class="fa fa-pencil"> NUEVO </i></button></a></h3>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="tabla-responsive">
      <table id="tabla" class="table table-hover">
        <thead class="fondo">
          <tr>
               <th>PREPARACIÓN DEL MENÚ</th>
            <th>NOTA</th>
            <th>ESTADO</th>
            <th>OPCIONES</th>
     
          </tr>
        </thead>
        <tbody>
          @foreach($menu as $men)
          <tr>

            <td>{{$men->menu}}</td>
            <td>{{$men->nota}}</td>
            @if($men->estado=="Activo")
            <td> <span class="label label-success">Activo</span></td>
            <td>
             <a href="{{URL::action('PreparacionController@show',$men->idmenu)}}" ><button class="btn btn-primary" title="DEVOLUCIÓN" ><i class=" fa fa-eye"></i></button></a>
              <a href="{{URL::action('PreparacionController@edit',$men->idmenu)}}" ><button class="btn btn-info" title="DEVOLUCIÓN" ><i class=" fa fa-edit"></i></button></a>
              <a href="" data-target="#modal-Delete-{{$men->idpreparacion}}" data-toggle="modal" ><button class="btn btn-danger" title="ELIMINAR" ><i class="fa fa-trash"></i></button></a>
            </td>
            @else
            <td><span class="label label-danger">Inactivo</span></td>
            <td >
              <a href="" data-target="#modal-Restore-{{$men->idmenu}}" data-toggle="modal"><button class="btn btn-warning" >RECUPERAR</button></a>
            </td>
            @endif
          </tr>
          @include('almacen.preparacion.modalDelete')
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
@endsection