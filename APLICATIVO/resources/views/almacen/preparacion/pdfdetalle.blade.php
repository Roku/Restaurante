
<h3 style="text-align:center;color:#2C4E79; font-weight: bold;">CREAR PROCEDIMIENTO DE LA RECETA {{ $menu->menu }}</h3>
<br>

<div class="row">
  
  <div class="col-lg-6 col-sm-3 col-md-3 col-xs-12">
    <div class="panel panel-primary ">
      <div class="panel-heading">
        <h3 class="panel-title">RECETA</h3>
      </div>
      <div class="panel-body">
        <table  class="table table-striped table-bordered table-condensed table-hover">
          <thead style="background-color:#A9D0F5 ">
            <tr>
              
              <th>Unidad metrica</th>
              <th>Ingredientes</th>
              
            </tr>
          </thead>
          <tbody>
            @foreach($detalles as $det)
            <tr>
              <td>{{$det->cantidad}}-{{ $det->medida }}</td>
              <td>{{$det->ingrediente}}</td>    
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-lg-6 col-sm-3 col-md-3 col-xs-12">
    <div class="panel panel-primary ">
      <div class="panel-heading">
        <h3 class="panel-title">PROCEDIMIENTO</h3>
      </div>
      <div class="panel-body">
        <div class="col-lg-12 col-sm-3 col-md-3 col-xs-12">
       <textarea class="form-control" rows="15" id="comment" name="preparacion" style="width: 700px; height: 250px;" >{{$preparacion->preparacion }}
<br>
<br>
       </textarea> <br>  
       <br>   
        </div>
             <div class="form-group">
    <label for="descripcion">Nota: </label>
    <input type="text" name="descripcion" class="form-control" placeholder="NOTA..." style="width: 600px; height: 100px;" value="{{ $preparacion->descripcion }}">  
    </div>
       
      </div>
    </div>
    
  </div>



