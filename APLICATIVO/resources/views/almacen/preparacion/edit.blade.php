@extends ('layouts.admin')
@section ('contenido')

<div class="content">
  <div class="row">
  <h3 style="text-align:center;color:#2C4E79; font-weight: bold;">CREAR PROCEDIMIENTO DE LA RECETA {{ $menu->menu }}</h3>
  <div class="col-lg-6 col-sm-3 col-md-3 col-xs-12">
    <div class="panel panel-primary ">
      <div class="panel-heading">
        <h3 class="panel-title">RECETA</h3>
      </div>
      <div class="panel-body">
        <table  class="table table-striped table-bordered table-condensed table-hover">
          <thead style="background-color:#A9D0F5 ">
            <tr>
              
              <th>Unidad metrica</th>
              <th>Ingredientes</th>
              
            </tr>
          </thead>
          <tbody>
            @foreach($detalles as $det)
            <tr>
              <td>{{$det->cantidad}}-{{ $det->medida }}</td>
              <td>{{$det->ingrediente}}</td>    
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  {{ Form::model($preparacion,['method'=>'PATCH','action'=>['PreparacionController@update',$preparacion->idpreparacion],'autocomplete'=>'off']) }}
  {{ form::token() }}

    {!!Form::token()!!}
<input type="" name="idmenu" value="{{ $menu->idmenu }}" style="display: none">

  <div class="col-lg-6 col-sm-3 col-md-3 col-xs-12">
    <div class="panel panel-primary ">
      <div class="panel-heading">
        <h3 class="panel-title">PROCEDIMIENTO</h3>
      </div>
      <div class="panel-body">
        <div class="col-lg-12 col-sm-3 col-md-3 col-xs-12">
       <textarea class="form-control" rows="11" id="comment" name="preparacion">{{$preparacion->preparacion }}</textarea> 
       <br>   
        </div>
             <div class="form-group">
    <label for="descripcion">Nota: </label>
    <input type="text" name="descripcion" class="form-control" placeholder="NOTA..." value="{{ $preparacion->descripcion }}">  
    </div>
        <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Confirmar</button>
      </div>
      </div>
    </div>
    
  </div>

</div>
  {!! Form::close() !!}
  @endsection  
