@extends ('layouts.admin')
@section ('contenido')

<div class="content">
  <div class="row">
   <a href="{{ URL('reportedetallepreparacion',$menu->idmenu) }}" ><button class="btn btn-warning" title="IMPRIMIR"><i class="fa fa-print"></i></button></a> 

  <h3 style="text-align:center;color:#2C4E79; font-weight: bold;">CREAR PROCEDIMIENTO DE LA RECETA {{ $menu->menu }}</h3>
  <div class="col-lg-6 col-sm-3 col-md-3 col-xs-12">
    <div class="panel panel-primary ">
      <div class="panel-heading">
        <h3 class="panel-title">RECETA</h3>
      </div>
      <div class="panel-body">
        <table  class="table table-striped table-bordered table-condensed table-hover">
          <thead style="background-color:#A9D0F5 ">
            <tr>
              
              <th>Unidad metrica</th>
              <th>Ingredientes</th>
              
            </tr>
          </thead>
          <tbody>
            @foreach($detalles as $det)
            <tr>
              <td>{{$det->cantidad}}-{{ $det->medida }}</td>
              <td>{{$det->ingrediente}}</td>    
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-lg-6 col-sm-3 col-md-3 col-xs-12">
    <div class="panel panel-primary ">
      <div class="panel-heading">
        <h3 class="panel-title">PROCEDIMIENTO</h3>
      </div>
      <div class="panel-body">
        <div class="col-lg-12 col-sm-3 col-md-3 col-xs-12">
       <textarea class="form-control" rows="13" id="comment" name="preparacion" readonly="">{{$preparacion->preparacion }}</textarea> 
       <br>   
        </div>
             <div class="form-group">
    <label for="descripcion">Nota: </label>
    <input type="text" name="descripcion" class="form-control" readonly="" placeholder="NOTA..." value="{{ $preparacion->descripcion }}">  
    </div>
       
      </div>
    </div>
    
  </div>
  
</div>
</div>
  @endsection  
