@extends('layouts.admin')
@section('contenido')


<div class="row">
  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <br>
    <h3>&nbsp&nbsp&nbspLISTADO DE MENÚ.</h3>
  </div>
</div>


<div class="row" >
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="tabla-responsive">
      <table id="tabla" class="table table-hover">
        <thead class="fondo">
          <tr>
            <th>MENÚ</th>
            <th>CATEGORÍA</th>
            <th>FECHA</th>
            <th>ESTADO</th>
            <th>OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($menu as $men)
          <tr>
            <td>{{$men->nombre}}</td>
             <td>{{$men->categoria}}</td>
            <td>{{$men->fecha}}</td>
            @if($men->estado=="Activo")
            <td> <span class="label label-success">Activo</span></td>
            <td>
              <a href="{{URL::action('PreparacionController@create',$men->idmenu)}}" ><button class="btn btn-primary" title="DEVOLUCIÓN" ><i class=" fa fa-mail-reply-all"></i></button></a>
            </td>
            @else
            <td><span class="label label-danger">Inactivo</span></td>
            <td >
               <a href="" data-target="#modal-Restore-{{$men->idmenu}}" data-toggle="modal"><button class="btn btn-warning" >RECUPERAR</button></a>
            </td>
            @endif
          </tr>

          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection