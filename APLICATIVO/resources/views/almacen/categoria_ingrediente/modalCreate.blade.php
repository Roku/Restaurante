<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" id="modal-Create">
	{{Form::Open(array('action'=>array('Categoria_IngredienteController@store'),'method'=>'POST'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #356c8c;">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">CREAR CATEGORÍA INGREDIENTE</font></h3>
			</div>
			<div class="modal-body">
				
{!!Form::token()!!}

    <div class="form-group">
    <label for="nombre">Nombre: </label>
    <input type="text" name="nombre" class="form-control" placeholder="Categoría Ingrediente...">	
    </div>
         <div class="form-group">
     <label>Impuesto:</label>
   
        <select name="idimpuesto"  class="form-control selectpicker" data-live-search="true">
        @foreach($impuesto as $imp)
              <option value="{{ $imp->idimpuesto }}">{{ $imp->impuesto }}</option>
      @endforeach
          </select>  
    </div>
     <div class="form-group">
    <label for="descripcion">Descripción: </label>
    <input type="text" name="descripcion" class="form-control" placeholder="Descripción...">	
    </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
