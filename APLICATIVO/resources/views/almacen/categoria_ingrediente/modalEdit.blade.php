<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" id="modal-Edit-{{$cat->idcategoria_ingrediente}}">
{{Form::Open(array('action'=>array('Categoria_IngredienteController@update',$cat->idcategoria_ingrediente),'method'=>'PATCH'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #356c8c;">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">EDITAR CATEGORÍA INGREDIENTE</font></h3>
			</div>
			<div class="modal-body">


    <div class="form-group">
    <label for="nombre">Categoría: </label>
    <input type="text" name="nombre" class="form-control" placeholder="Categoría ingrediente..." value="{{ $cat->nombre }}" >	
    </div>
        <div class="form-group">
                            <label>Impuesto</label>
                            
                            <select class=" form-control " data-live-search="true" name="idcategoria" >
                                @foreach($impuesto as $imp)
                                @if($imp->idimpuesto==$cat->idimpuesto)
                                <option value="{{ $imp->idimpuesto }}" selected >{{ $imp->impuesto }}</option>
                                @else
                                <option value="{{ $imp->idimpuesto }}">{{ $imp->impuesto }}</option>
                                
                                @endif
                                @endforeach
                                
                            </select>
                            
                        </div>
     <div class="form-group">
    <label for="descripcion">Descripción: </label>
    <input type="text" name="descripcion" class="form-control" placeholder="Descripcion..."  value="{{ $cat->descripcion }}">	
    </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
