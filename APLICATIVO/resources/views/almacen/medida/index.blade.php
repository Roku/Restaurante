@extends('layouts.admin')
@section('contenido')

@include('almacen.medida.modalCreate')
@include('flash::message')
<div class="content">
  <div class="row" >
      <div class="col-lg-10 col-md-10 col-sm-10">
        <br>
        <h3>&nbsp&nbsp&nbspLISTADO DE MEDIDA. <a href="" data-target="#modal-Create" data-toggle="modal" ><button class="btn btn-primary" >NUEVO</button></a> </h3>
      </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="tabla-responsive">
      <table id="tabla" class="table table-hover">
        <thead class="fondo">
          <tr>
            
            <th>MEDIDA</th>
            <th>DESCRIPCIÓN</th>
            <th>ESTADO</th>
            <th>OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($medida as $med)
          <tr>
            <td>{{$med->nombre}}</td>
            <td>{{$med->descripcion}}</td>
            @if($med->estado=="Activo")
            <td> <span class="label label-success">Activo</span></td>
            <td>
              <a href="" data-target="#modal-Edit-{{$med->idunidad_medida}}" data-toggle="modal" ><button class="btn btn-info" title="EDITAR" ><i class="fa fa-edit"></i></button></a>
              <a href="" data-target="#modal-Delete-{{$med->idunidad_medida}}" data-toggle="modal" ><button class="btn btn-danger" title="ELIMINAR" ><i class="fa fa-trash"></i></button></a>
            </td>
            @else
            <td><span class="label label-danger">Inactivo</span></td>
            <td >
               <a href="" data-target="#modal-Restore-{{$med->idunidad_medida}}" data-toggle="modal"><button class="btn btn-warning" >RECUPERAR</button></a>
            </td>
            @endif
          </tr>
    @include('almacen.medida.modalEdit')
    @include('almacen.medida.modalDelete')  
    @include('almacen.medida.modalRestore')
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
<br>
</div>
@endsection