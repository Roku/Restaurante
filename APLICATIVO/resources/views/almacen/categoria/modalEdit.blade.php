<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" id="modal-Edit-{{$cat->idcategoria}}">
{{Form::Open(array('action'=>array('CategoriaController@update',$cat->idcategoria),'method'=>'PATCH'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #356c8c;">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">EDITAR CATEGORÍA</font></h3>
			</div>
			<div class="modal-body">


    <div class="form-group">
    <label for="nombre">Nombre: </label>
    <input type="text" name="nombre" class="form-control" placeholder="Nombre..." value="{{ $cat->nombre }}">	
    </div>
     <div class="form-group">
    <label for="descripcion">Descripción: </label>
    <input type="text" name="descripcion" class="form-control" placeholder="Descripcion..."  value="{{ $cat->descripcion }}" >	
    </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
