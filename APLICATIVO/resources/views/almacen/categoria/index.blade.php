@extends('layouts.admin')
@section('contenido')
<div class="row">
  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <br>
    <h3>&nbsp&nbspLISTADO DE CATEGORÍAS PARA MENÚ. <a href="" data-target="#modal-Create" data-toggle="modal" ><button class="btn btn-primary" >NUEVO</button></a> </h3>
  </div>
</div>
@include('almacen.categoria.modalCreate')
@include('flash::message')
<div class="content">
  <div class="row" >
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="tabla-responsive">
      <table id="tabla" class="table table-hover">
        <thead class="fondo">
          <tr>
            
            <th>CATEGORÍA</th>
            <th>DESCRIPCIÓN</th>
            <th>ESTADO</th>
            <th>OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($categoria as $cat)
          <tr>
            <td>{{$cat->nombre}}</td>
            <td>{{$cat->descripcion}}</td>
            @if($cat->estado=="Activo")
            <td> <span class="label label-success">Activo</span></td>
            <td>
              <a href="" data-target="#modal-Edit-{{$cat->idcategoria}}" data-toggle="modal" ><button class="btn btn-info" title="EDITAR" ><i class="fa fa-edit"></i></button></a>
              <a href="" data-target="#modal-Delete-{{$cat->idcategoria}}" data-toggle="modal" ><button class="btn btn-danger" title="ELIMINAR" ><i class="fa fa-trash"></i></button></a>
            </td>
            @else
            <td><span class="label label-danger">Inactivo</span></td>
            <td >
               <a href="" data-target="#modal-Restore-{{$cat->idcategoria}}" data-toggle="modal"><button class="btn btn-warning" >RECUPERAR</button></a>
            </td>
            @endif
          </tr>
          @include('almacen.categoria.modalRestore')
          @include('almacen.categoria.modalEdit')
          @include('almacen.categoria.modalDelete')
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
<br>
</div>

@endsection