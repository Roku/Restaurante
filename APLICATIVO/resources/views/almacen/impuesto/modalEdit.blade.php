<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" id="modal-Edit-{{$imp->idimpuesto}}">
{{Form::Open(array('action'=>array('ImpuestoController@update',$imp->idimpuesto),'method'=>'PATCH'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #356c8c;">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">EDITAR IMPUESTO</font></h3>
			</div>
			<div class="modal-body">


    <div class="form-group">
    <label for="nombre">Impuesto: </label>
    <input type="text" name="impuesto" class="form-control" placeholder="Impuesto..." value="{{ $imp->impuesto }}" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">	
    </div>
     <div class="form-group">
    <label for="descripcion">Descripción: </label>
    <input type="text" name="descripcion" class="form-control" placeholder="Descripcion del impuesto..."  value="{{ $imp->descripcion }}">	
    </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
