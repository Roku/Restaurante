<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" id="modal-Create">
	{{Form::Open(array('action'=>array('ImpuestoController@store'),'method'=>'POST'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #356c8c;">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">CREAR IMPUESTO</font></h3>
			</div>
			<div class="modal-body">
				
{!!Form::token()!!}

    <div class="form-group">
    <label for="nombre">Impuesto: </label>
    <input type="text" name="impuesto" class="form-control" placeholder="Impuesto..." >	
    </div>
     <div class="form-group">
    <label for="descripcion">Descripción: </label>
    <input type="text" name="descripcion" class="form-control" placeholder="Descripcion del impuesto..." >	
    </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
