<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-Delete-{{$ingr->idingrediente}}">
	{{Form::Open(array('action'=>array('IngredienteController@destroy',$ingr->idingrediente),'method'=>'DELETE'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #d9534f;">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">ELIMINAR INGREDIENTE</font></h3>
			</div>
			<div class="modal-body">
				<p><font size="4">Confirme si desea Eliminar este ingrediente</font></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>