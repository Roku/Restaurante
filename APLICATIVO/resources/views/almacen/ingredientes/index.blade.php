 
@extends('layouts.admin')
@section('contenido')
@include('almacen.ingredientes.modalCreate')
@include('flash::message')
<div class="content">
  <div class="row" >
  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <br>
    <h3>&nbsp&nbsp&nbspLISTADO DE INGREDIENTES. <a href="" data-target="#modal-Create" data-toggle="modal" ><button class="btn btn-primary" >NUEVO</button></a> </h3>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="tabla-responsive">
      <table id="tabla" class="table table-hover">
        <thead class="fondo">
          <tr>
            
            <th>INGREDIENTE</th>
              <th>CATEGORÍA</th>
            <th>ESTADO</th>
            <th>OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($ingrediente as $ingr)
          <tr>
            <td>{{$ingr->nombre}}</td>
             <td>{{$ingr->categoria}}</td>
            @if($ingr->estado=="Activo")
            <td> <span class="label label-success">Activo</span></td>
           <td>
              <a href="" data-target="#modal-Edit-{{$ingr->idingrediente}}" data-toggle="modal" ><button class="btn btn-info" title="EDITAR" ><i class="fa fa-edit"></i></button></a>
              <a href="" data-target="#modal-Delete-{{$ingr->idingrediente}}" data-toggle="modal" ><button class="btn btn-danger" title="ELIMINAR" ><i class="fa fa-trash"></i></button></a>
              
            </td>
            @else
            <td>
              <span class="label label-danger">Inactivo</span>
            </td>
            <td>
               <a href="" data-target="#modal-Restore-{{$ingr->idingrediente}}" data-toggle="modal"><button class="btn btn-warning" >RECUPERAR</button></a>
            </td>            
            @endif
          </tr>
    @include('almacen.ingredientes.modalEdit')
    @include('almacen.ingredientes.modalDelete')
    @include('almacen.ingredientes.modalRestore')
        @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
<br>
</div>
@endsection
