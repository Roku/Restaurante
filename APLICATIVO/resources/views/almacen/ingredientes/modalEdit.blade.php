<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" id="modal-Edit-{{$ingr->idingrediente}}">
{{Form::Open(array('action'=>array('IngredienteController@update',$ingr->idingrediente),'method'=>'PATCH'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #356c8c;">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">EDITAR INGREDIENTE</font></h3>
			</div>
			<div class="modal-body">


    <div class="form-group">
    <label for="nombre">Ingrediente: </label>
    <input type="text" name="nombre" class="form-control" placeholder="Ingrediente..." value="{{ $ingr->nombre }}" >	
    </div>
       <div class="form-group">
                            <label>Categoria</label>
                            
                            <select class=" form-control " data-live-search="true" name="idcategoria_ingrediente" >
                                @foreach($categoria_ingrediente as $cat)
                                @if($cat->idcategoria_ingrediente==$ingr->idcategoria_ingrediente)
                                <option value="{{ $cat->idcategoria_ingrediente }}" selected >{{ $cat->nombre }}</option>
                                @else
                                <option value="{{ $cat->idcategoria_ingrediente }}">{{ $cat->nombre }}</option>
                                
                                @endif
                                @endforeach
                                
                            </select>
                            
                        </div>
     <div class="form-group" style="display: none;">
    <label for="descripcion">Descripción: </label>
    <input type="text" name="descripcion" class="form-control" placeholder="Descripcion..."  value="{{ $ingr->descripcion }}" >	
    </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>