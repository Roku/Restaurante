@extends('layouts.admin')
@section('contenido')

@include('almacen.proveedor.modalCreate')
@include('flash::message')
<div class="content">
  <div class="row" >
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <br>
    <h3>&nbsp&nbsp&nbspLISTADO DE PROVEEDORES. <a href="" data-target="#modal-Create" data-toggle="modal" ><button class="btn btn-primary" >NUEVO</button></a> </h3>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="tabla-responsive">
      <table id="tabla" class="table table-hover">
        <thead class="fondo">
          <tr>
            
            <th>EMPRESA</th>
             <th>NOMBRE Y APELLIDO</th>
            <th>DOCUMENTO</th>
            <th>DIRECCIÓN</th>
            <th>TELÉFONO</th>
            <th>EMAIL</th>
            <th>ESTADO</th>
            <th>OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach($proveedor as $pro)
          <tr>
            <td>{{$pro->empresa}}</td>
            <td>{{$pro->nombre}}-{{$pro->apellido}}</td>
            <td>{{$pro->num_documento}}</td>
            <td>{{$pro->direccion}}</td>
            <td>{{$pro->telefono}}</td>
            <td>{{$pro->email}}</td>
            @if($pro->estado=="Activo")
            <td> <span class="label label-success">Activo</span></td>
            <td>
              <a href="" data-target="#modal-Edit-{{$pro->idproveedor}}" data-toggle="modal" ><button class="btn btn-info" title="EDITAR" ><i class="fa fa-edit"></i></button></a>
              <a href="" data-target="#modal-Delete-{{$pro->idproveedor}}" data-toggle="modal" ><button class="btn btn-danger" title="ELIMINAR" ><i class="fa fa-trash"></i></button></a>
            </td>
            @else
            <td><span class="label label-danger">Inactivo</span></td>
            <td >
               <a href="" data-target="#modal-Restore-{{$pro->idproveedor}}" data-toggle="modal"><button class="btn btn-warning" >RECUPERAR</button></a>
            </td>
            @endif
          </tr>
          @include('almacen.proveedor.modalRestore')
          @include('almacen.proveedor.modalEdit')
          @include('almacen.proveedor.modalDelete')
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>

@endsection