<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" id="modal-Edit-{{$cli->idcliente}}">
{{Form::Open(array('action'=>array('ClienteController@update',$cli->idcliente),'method'=>'PATCH'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #356c8c;">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">EDITAR CLIENTE</font></h3>
			</div>
			<div class="modal-body">

 <div class="row"> 
<div class="col-sm-6">  
 <div class="form-group">
    <label for="nombre">Nombre: </label>
    <input type="text" name="nombre" class="form-control" placeholder="Nombres..."  value="{{ $cli->nombre }}"> 
    </div>
</div>
<div class="col-sm-6">  
    <div class="form-group">
    <label for="nombre">Apellido: </label>
    <input type="text" name="apellido" class="form-control" placeholder="Apellidos..."  value="{{ $cli->apellido }}"> 
    </div>
</div>
</div>
     <div class="form-group">
    <label for="nombre">Cédula o RUC: </label>
    <input type="text" name="num_documento" class="form-control" placeholder="Cédula o RUC..."  value="{{ $cli->apellido }}">	
    </div>
     <div class="form-group">
    <label for="descripcion">Teléfono: </label>
    <input type="text" name="telefono" class="form-control" placeholder="Teléfono o celular..."  value="{{ $cli->telefono }}">	
    </div>
      <div class="form-group">
    <label for="descripcion">Dirección: </label>
    <input type="text" name="direccion" class="form-control" placeholder="Dirección domiciliaria..."  value="{{ $cli->direccion }}">	
    </div>
      <div class="form-group">
    <label for="descripcion">Email: </label>
    <input type="text" name="email" class="form-control" placeholder="Correo Electrónico..."  value="{{ $cli->email }}">	
    </div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
