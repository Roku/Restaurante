<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" id="modal-Edit-{{$dia->iddia_especial}}">
    {{Form::Open(array('action'=>array('Dia_EspecialController@update',$dia->iddia_especial),'method'=>'PATCH','files'=>'true'))}}
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background: #356c8c;">
                <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">EDITAR DÍA ESPECIAL</font></h3>
            </div>
            <div class="modal-body">
                {{Form::token()}}
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label for="nombre">Día especial: </label>
                            <input type="text" name="nombre" class="form-control" value="{{$dia->nombre}}" placeholder="Día especial..." >
                        </div>
                        <div class="form-group">
                            <label for="descripcion">Descripción: </label>
                               <textarea class="form-control" rows="" id="comment" name="descripcion" value="" placeholder="Descripción del día especial o promociones...">{{ $dia->descripcion }}</textarea> 
                        </div>
                        
                        <div class="form-group">
                            <label for="imagen">Imagen uno:</label>
                            <input type="file" name="imagen"  class="form-control">
                            
                        </div>
                           <div class="form-group">
                            <label for="imagen">Imagen dos:</label>
                            <input type="file" name="imagendos"  class="form-control">
                            
                        </div>
                           <div class="form-group">
                            <label for="imagen">Imagen tres:</label>
                            <input type="file" name="imagentres"  class="form-control">
                            
                        </div>
                           <div class="form-group">
                            <label for="imagen">Imagen cuatro:</label>
                            <input type="file" name="imagencuatro"  class="form-control">
                            
                        </div>
                    </div>

        <section id="story" class="description_content">
               <div class="text-content container">
      
                          <div class="col-md-7">
                    <div class="img-section">
                       @if(($dia->imagen)!="")
                            <img src="{{asset('imagenes/dia_especial/'.$dia->imagen)}}"  width="244" height="215" style="border-radius: 8px; box-shadow: 2px 4px 6px rgba(0,0,0,0.5)">.
                            @endif
                       @if(($dia->imagen)!="")
                            <img src="{{asset('imagenes/dia_especial/'.$dia->imagendos)}}"  width="244" height="215" style="border-radius: 8px; box-shadow: 2px 4px 6px rgba(0,0,0,0.5)">
                            @endif
                       <div class="img-section-space"> <br></div>
                    @if(($dia->imagen)!="")
                            <img src="{{asset('imagenes/dia_especial/'.$dia->imagentres)}}"  width="244" height="215" style="border-radius: 8px; box-shadow: 2px 4px 6px rgba(0,0,0,0.5)">.
                            @endif
                       @if(($dia->imagen)!="")
                            <img src="{{asset('imagenes/dia_especial/'.$dia->imagencuatro)}}"  width="244" height="215" style="border-radius: 8px; box-shadow: 2px 4px 6px rgba(0,0,0,0.5)">
                            @endif
                   </div>
                </div>
                </div>
                     </section>

                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Confirmar</button>
            </div>
        </div>
    </div>
    {{Form::Close()}}
</div>