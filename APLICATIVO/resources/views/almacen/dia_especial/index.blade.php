@extends('layouts.admin')
@section('contenido')
@include('almacen.dia_especial.modalCreate')

<div class="content">
  <div class="row" >
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <br>
    <h3>&nbsp&nbsp&nbspLISTADO DE DÍA ESPECIAL. <a href="" data-target="#modal-Create" data-toggle="modal" ><button class="btn btn-primary" >NUEVO</button></a> </h3>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="tabla-responsive">
      <table id="tabla" class="table table-hover">
        <thead class="fondo">
          <tr>
            
            <th>DÍA ESPECIAL</th>
            <th>IMAGEN UNO</th>
            <th>IMAGEN DOS</th>
            <th>IMAGEN TRES</th>
            <th>IMAGEN CUATRO</th>
            <th>ESTADO</th>
            <th>OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          @foreach( $dia_especial as $dia)
          <tr>
            <td>{{$dia->nombre}}</td>
             <td>
           <img src="{{asset('imagenes/dia_especial/'.$dia->imagen)}}" alt="{{$dia->nombre}}" height="10px" width="80px" class="img-thumbnail">

        </td>
          <td>
           <img src="{{asset('imagenes/dia_especial/'.$dia->imagendos)}}" alt="{{$dia->nombre}}" height="10px" width="80px" class="img-thumbnail">

        </td>
          <td>
           <img src="{{asset('imagenes/dia_especial/'.$dia->imagentres)}}" alt="{{$dia->nombre}}" height="10px" width="80px" class="img-thumbnail">

        </td>
          <td>
           <img src="{{asset('imagenes/dia_especial/'.$dia->imagencuatro)}}" alt="{{$dia->nombre}}" height="10px" width="80px" class="img-thumbnail">

        </td>
            @if($dia->estado=="Activo")
            <td> <span class="label label-success">Activo</span></td>
            <td>
              <a href="" data-target="#modal-Edit-{{$dia->iddia_especial}}" data-toggle="modal" ><button class="btn btn-info" title="EDITAR" ><i class="fa fa-edit"></i></button></a>
              <a href="" data-target="#modal-Delete-{{$dia->iddia_especial}}" data-toggle="modal" ><button class="btn btn-danger" title="ELIMINAR" ><i class="fa fa-trash"></i></button></a>
            </td>
            @else
            <td><span class="label label-danger">Inactivo</span></td>
            <td >
               <a href="" data-target="#modal-Restore-{{$dia->iddia_especial}}" data-toggle="modal"><button class="btn btn-warning" >RECUPERAR</button></a>
            </td>
            @endif
          </tr>
        @include('almacen.dia_especial.modalEdit')
        @include('almacen.dia_especial.modalDelete')
        @include('almacen.dia_especial.modalRestore')
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
@endsection