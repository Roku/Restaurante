<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" id="modal-Create">
	{{Form::Open(array('action'=>array('Dia_EspecialController@store'),'method'=>'POST','files'=>'true'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #356c8c;">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">CREAR DÍA ESPECIAL</font></h3>
			</div>
			<div class="modal-body">
				
{!!Form::token()!!}
    <div class="form-group">
    <label for="nombre">Día especial: </label>
    <input type="text" name="nombre" class="form-control" placeholder="Día especial..." value="{{ old('nombre') }}">	
    </div>
     <div class="form-group">
    <label for="descripcion">Descripción: </label>
    <textarea class="form-control" rows="" id="comment" name="descripcion" placeholder="Descripción del día especial o promociones"></textarea> 
    </div>
     <div class="form-group">
        <label for="imagen">Imagen uno:</label>
        <input type="file" name="imagen"  class="form-control">
      </div>
       <div class="form-group">
        <label for="imagen">Imagen dos:</label>
        <input type="file" name="imagendos"  class="form-control">
      </div>
       <div class="form-group">
        <label for="imagen">Imagen tres:</label>
        <input type="file" name="imagentres"  class="form-control">
      </div>
       <div class="form-group">
        <label for="imagen">Imagen cuatro:</label>
        <input type="file" name="imagencuatro"  class="form-control">
      </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
