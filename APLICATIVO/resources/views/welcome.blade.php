<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title>Restaurant</title>
        <link rel="stylesheet" href="{{asset('jiva/css/normalize.css')}}">
        <link rel="stylesheet" href="{{asset('jiva/css/main.css')}}" media="screen" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{asset('jiva/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('jiva/css/style-portfolio.css')}}">
        <link rel="stylesheet" href="{{asset('jiva/css/picto-foundry-food.css')}}" />
        <link rel="stylesheet" href="{{asset('jiva/css/jquery-ui.css')}}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="{{asset('jiva/css/font-awesome.min.css')}}" rel="stylesheet">

    <style type="text/css" media="screen">
        .caret{
            color: #fff;
        }
        .dropdown{
            color: #fff;
        }
        .dropdown a:active{
            color: #fff;
        }
        .collapse .navbar-collapse .dropdown .dropdown-toggle{

        }
        .dropdown .dropdown-menu > ul > li >a:hover{
            color: red;
        }
    </style>
    </head>

    <body>
    </head>

    <body>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Restaurant</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav main-nav  clear navbar-right ">
                            <li><a class="navactive color_animation" href="#top">WELCOME</a></li>
                            <li><a class="color_animation" href="#story">ABOUT</a></li>
                            <li><a class="color_animation" href="#pricing">PRICING</a></li>
                            <li><a class="color_animation" href="#beer">BEER</a></li>
                            <li><a class="color_animation" href="#bread">BREAD</a></li>
                            <li><a class="color_animation" href="#featured">FEATURED</a></li>
                            <li><a class="color_animation" href="#reservation">RESERVATION</a></li>
                            <li><a class="color_animation" href="#contact">CONTACT</a></li>

                            <li>
                           <!-- Right Side Of Navbar -->
                                <ul class="nav navbar-nav main-nav  clear navbar-right ">
                                    <!-- Authentication Links -->
                                    @guest
                                        <li>
                                            <a class="color_animation" href="{{ route('login') }}">Login</a>
                                        </li>
                                        <li>
                                            <a class="color_animation" href="{{ route('register') }}">Register</a>
                                        </li>
                                    @else
                                        <li class="dropdown">
                                            <a href="#"  style="color: #fff;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('logout') }}"
                                                        onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                                        Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                    @endguest
                                </ul>
                                </li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </div><!-- /.container-fluid -->
        </nav>
         
        <div id="top" class="starter_container bg">
            <div class="follow_container">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="top-title"> Restaurant</h2>
                    <h2 class="white second-title">" Best in the city "</h2>
                    <hr>
                </div>
            </div>
        </div>

        <!-- ============ About Us ============= -->

        <section id="story" class="description_content">
            <div class="text-content container">
                <div class="col-md-6">

                    <h1 class="top-title-about">About us</h1>

                    <div class="fa fa-cutlery fa-2x"></div>
                    <p class="desc-text">Restaurant is a place for simplicity. Good food, good beer, and good service. Simple is the name of the game, and we’re good at finding it in all the right places, even in your dining experience. We’re a small group from Denver, Colorado who make simple food possible. Come join us and see what simplicity tastes like.</p>
                </div>
                <div class="col-md-6">
                    <div class="img-section">
                       <img src="{{asset('jiva/images/kabob.jpg')}}" width="250" height="220">
                       <img src="{{asset('jiva/images/limes.jpg')}}" width="250" height="220">
                       <div class="img-section-space"></div>
                       <img src="{{asset('jiva/images/radish.jpg')}}"  width="250" height="220">
                       <img src="{{asset('jiva/images/corn.jpg')}}"  width="250" height="220">
                   </div>
                </div>
            </div>
        </section>


       <!-- ============ Pricing  ============= -->


        <section id ="pricing" class="description_content">
             <div class="pricing background_content">

                <h1 class="top-title-about"><span >Affordable</span> pricing</h1>
             </div>
            <div class="text-content container"> 
            <div class="container">
        <div class="row">
        <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="gallery-title top-title-about">NUESTRO PRODUCTO</h1>
        </div>
      
        <div class="menu-galery">

            <button class="text-center form-btn form-btn btn-default filter-button" data-filter="all">All</button>
            <button class="text-center form-btn form-btn btn-default filter-button" data-filter="hdpe">HDPE Pipes</button>
            <button class="text-center form-btn form-btn btn-default filter-button" data-filter="sprinkle">Sprinkle Pipes</button>
            <button class="text-center form-btn form-btn btn-default filter-button" data-filter="spray">Spray Nozzle</button>
            <button class="text-center form-btn form-btn btn-default filter-button" data-filter="irrigation">Irrigation Pipes</button>
        </div>
        
        <div id="mixcontent" class="container-galery">
            <div class="col-md-4 mix filter hdpe no-padding">
                <div class="single_mixi_portfolio">
                    <img src="{{asset('images/beer.jpg')}}" alt="" />
                     <div class="mixi_portfolio_overlay">
                        <div class="overflow_hover_text">
                             <h2>WILLY</h2>
                             <p>ARE IMPORTANT FOR COOKING ?</p>
                                          
                              <a type="button"><i class="lnr lnr-plus-circle" data-toggle="modal" data-target="#myModal"></i></a>
                         </div>
                    </div>
                 </div>
            </div> 


            <div class="col-md-4 mix filter sprinkle no-padding">
                <div class="single_mixi_portfolio">
                    <img src="{{asset('images/slider1.jpg')}}" alt="" />
                     <div class="mixi_portfolio_overlay">
                        <div class="overflow_hover_text">
                             <h2>INGREDIENTS</h2>
                             <p>ARE IMPORTANT FOR COOKING ?</p>
                                          
                              <a type="button"><i class="lnr lnr-plus-circle" data-toggle="modal" data-target="#myModal"></i></a>
                         </div>
                    </div>
                 </div>
            </div>   
           
            <div class="col-md-4 mix filter hdpe no-padding">
                <div class="single_mixi_portfolio">
                    <img src="{{asset('images/slider2.jpg')}}" alt="" />
                     <div class="mixi_portfolio_overlay">
                        <div class="overflow_hover_text">
                             <h2>INGREDIENTS</h2>
                             <p>ARE IMPORTANT FOR COOKING ?</p>
                                          
                              <a type="button"><i class="lnr lnr-plus-circle" data-toggle="modal" data-target="#myModal"></i></a>
                         </div>
                    </div>
                 </div>
            </div>   
            
   
            <div class="col-md-4 mix filter spray no-padding">
                <div class="single_mixi_portfolio">
                    <img src="{{asset('images/beer.jpg')}}" alt="" />
                     <div class="mixi_portfolio_overlay">
                        <div class="overflow_hover_text">
                             <h2>INGREDIENTS</h2>
                             <p>ARE IMPORTANT FOR COOKING ?</p>
                                          
                              <a type="button"><i class="lnr lnr-plus-circle" data-toggle="modal" data-target="#myModal"></i></a>
                         </div>
                    </div>
                 </div>
            </div>  
            
            <div class="col-md-4 mix filter irrigation no-padding">
                <div class="single_mixi_portfolio">
                    <img src="{{asset('images/beer.jpg')}}" alt="" />
                     <div class="mixi_portfolio_overlay">
                        <div class="overflow_hover_text">
                             <h2>INGREDIENTS</h2>
                             <p>ARE IMPORTANT FOR COOKING ?</p>
                                          
                              <a type="button"><i class="lnr lnr-plus-circle" data-toggle="modal" data-target="#myModal"></i></a>
                         </div>
                    </div>
                 </div>
            </div>
           

            <div class="col-md-4 mix filter spray no-padding">
                <div class="single_mixi_portfolio">
                    <img src="{{asset('images/kabob.jpg')}}" alt="" />
                     <div class="mixi_portfolio_overlay">
                        <div class="overflow_hover_text">
                             <h2>INGREDIENTS</h2>
                             <p>ARE IMPORTANT FOR COOKING ?</p>
                                          
                              <a type="button"><i class="lnr lnr-plus-circle" data-toggle="modal" data-target="#myModal"></i></a>
                         </div>
                    </div>
                 </div>
            </div>

           <div class="col-md-4 mix filter irrigation no-padding">
                <div class="single_mixi_portfolio">
                    <img src="{{asset('images/steak.jpg')}}" alt="" />
                     <div class="mixi_portfolio_overlay">
                        <div class="overflow_hover_text">
                             <h2>INGREDIENTS</h2>
                             <p>ARE IMPORTANT FOR COOKING ?</p>
                                          
                              <a type="button"><i class="lnr lnr-plus-circle" data-toggle="modal" data-target="#myModal"></i></a>
                         </div>
                    </div>
                 </div>
            </div>

           
            <div class="col-md-4 mix filter hdpe no-padding">
                <div class="single_mixi_portfolio">
                    <img src="{{asset('images/slider2.jpg')}}" alt="" />
                     <div class="mixi_portfolio_overlay">
                        <div class="overflow_hover_text">
                             <h2>INGREDIENTS</h2>
                             <p>ARE IMPORTANT FOR COOKING ?</p>
                                          
                              <a type="button"><i class="lnr lnr-plus-circle" data-toggle="modal" data-target="#myModal"></i></a>
                         </div>
                    </div>
                 </div>
            </div>
            
             <div class="col-md-4 mix filter spray no-padding">
                <div class="single_mixi_portfolio">
                    <img src="{{asset('images/slider1.jpg')}}" alt="" />
                     <div class="mixi_portfolio_overlay">
                        <div class="overflow_hover_text">
                             <h2>INGREDIENTS</h2>
                             <p>ARE IMPORTANT FOR COOKING ?</p>
                                          
                              <a type="button"><i class="lnr lnr-plus-circle" data-toggle="modal" data-target="#myModal"></i></a>
                         </div>
                    </div>
                 </div>
            </div>

             <div class="col-md-4 mix filter sprinkle no-padding">
                <div class="single_mixi_portfolio">
                    <img src="{{asset('images/seeds.jpg')}}" alt="" />
                     <div class="mixi_portfolio_overlay">
                        <div class="overflow_hover_text">
                             <h2>INGREDIENTS</h2>
                             <p>ARE IMPORTANT FOR COOKING ?</p>
                                          
                              <a type="button"><i class="lnr lnr-plus-circle" data-toggle="modal" data-target="#myModal"></i></a>
                         </div>
                    </div>
                 </div>
            </div>
          

            </div>
        </div>
    </div>
</section>

            <div class="text-content container"> 
                <div class="container">
                    <div class="row">
                        <div id="w">
                            <ul id="filter-list" class="clearfix">
                                <li class="filter" data-filter="all">All</li>
                                <li class="filter" data-filter="breakfast">Breakfast</li>
                                <li class="filter" data-filter="special">Special</li>
                                <li class="filter" data-filter="desert">Desert</li>
                                <li class="filter" data-filter="dinner">Dinner</li>
                            </ul><!-- @end #filter-list -->    
                            <ul id="portfolio">
                                <li class="item breakfast">
                                    <img src="{{asset('images/food_icon01.jpg')}}" alt="Food" >
                                   
                                </li>

                                <li class="item dinner special"><img src="{{asset('images/food_icon02.jpg')}}" alt="Food" >
                                   
                                </li>
                                <li class="item dinner breakfast"><img src="{{asset('images/food_icon03.jpg')}}" alt="Food" >
                                    
                                </li>
                                <li class="item special "><img src="{{asset('images/food_icon04.jpg')}}" alt="Food" >
                                    
                                </li>
                                <li class="item dinner"><img src="{{asset('images/food_icon05.jpg')}}" alt="Food" >
                                   
                                </li>
                                <li class="item special"><img src="{{asset('images/food_icon06.jpg')}}" alt="Food" >
                                    
                                </li>
                                <li class="item desert"><img src="{{asset('images/food_icon07.jpg')}}" alt="Food" >
                                    
                                </li>
                                <li class="item desert breakfast"><img src="{{asset('images/food_icon08.jpg')}}" alt="Food" >
                                    
                                </li>
                            </ul><!-- @end #portfolio -->
                        </div><!-- @end #w -->
                    </div>
                </div>

            </div>  
        </section>


        <!-- ============ Our Beer  ============= -->


        <section id ="beer" class="description_content">

            <div class="ourbeer-container">
            <div  class="beer background_content">
                <h1>Great <span>Place</span> to enjoy</h1>
            </div>
            <div class="text-content container"> 
                <div class="col-md-5">
                   <div class="img-section">
                       <img src="images/beer_spec.jpg" width="100%">
                   </div>
                </div>
                <br>
                <div class="col-md-6 col-md-offset-1">
                    <h1>OUR BEER</h1>
                    <div class="icon-beer fa-2x"></div>
                    <p class="desc-text">Here at Restaurant we’re all about the love of beer. New and bold flavors enter our doors every week, and we can’t help but show them off. While we enjoy the classics, we’re always passionate about discovering something new, so stop by and experience our craft at its best.</p>
                </div>
            </div>

            </div>

        </section>


       <!-- ============ Our Bread  ============= -->


        <section id="bread" class=" description_content">
            <div  class="bread background_content">
                <h1>Our Breakfast <span>Menu</span></h1>
            </div>
            <div class="text-content container"> 
                <div class="col-md-6">
                    <h1>OUR BREAD</h1>
                    <div class="icon-bread fa-2x"></div>
                    <p class="desc-text">We love the smell of fresh baked bread. Each loaf is handmade at the crack of dawn, using only the simplest of ingredients to bring out smells and flavors that beckon the whole block. Stop by anytime and experience simplicity at its finest.</p>
                </div>
                <div class="col-md-6">
                    <img src="images/bread1.jpg" width="260" alt="Bread">
                    <img src="images/bread1.jpg" width="260" alt="Bread">
                </div>
            </div>
        </section>


        
        <!-- ============ Featured Dish  ============= -->

        <section id="featured" class="description_content">
            <div  class="featured background_content">
                <h1>Our Featured Dishes <span>Menu</span></h1>
            </div>
            <div class="text-content container"> 
                <div class="col-md-6">
                    <h1>Have a look to our dishes!</h1>
                    <div class="icon-hotdog fa-2x"></div>
                    <p class="desc-text">Each food is handmade at the crack of dawn, using only the simplest of ingredients to bring out smells and flavors that beckon the whole block. Stop by anytime and experience simplicity at its finest.</p>
                </div>
                <div class="col-md-6">
                    <ul class="image_box_story2">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="images/slider1.jpg"  alt="...">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="images/slider2.jpg" alt="...">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="images/slider3.JPG" alt="...">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
        </section>

        <!-- ============ Reservation  ============= -->

        <section  id="reservation"  class="description_content">
            <div class="featured background_content">
                <h1>Reserve a Table!</h1>
            </div>
            <div class="text-content container"> 
                <div class="inner contact">
                    <!-- Form Area -->
                    <div class="contact-form">
                        <!-- Form -->
                        <form id="contact-us" method="post" action="reserve.php">
                            <!-- Left Inputs -->
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-md-6 col-xs-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                                <!-- Name -->
                                                <input type="text" name="first_name" id="first_name" required="required" class="form" placeholder="First Name" />
                                                <input type="text" name="last_name" id="last_name" required="required" class="form" placeholder="Last Name" />
                                                <input type="text" name="state" id="state" required="required" class="form" placeholder="State" />
                                                <input type="text" name="datepicker" id="datepicker" required="required" class="form" placeholder="Reservation Date" />
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                                <!-- Name -->
                                                <input type="text" name="phone" id="phone" required="required" class="form" placeholder="Phone" />
                                                <input type="text" name="guest" id="guest" required="required" class="form" placeholder="Guest Number" />
                                                <input type="email" name="email" id="email" required="required" class="form" placeholder="Email" />
                                                <input type="text" name="subject" id="subject" required="required" class="form" placeholder="Subject" />
                                            </div>

                                            <div class="col-xs-6 ">
                                                <!-- Send Button -->
                                                <button type="submit" id="submit" name="submit" class="text-center form-btn form-btn">Reserve</button> 
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-4 col-md-6 col-xs-12">
                                        <!-- Message -->
                                        <div class="right-text">
                                            <h2>Hours</h2><hr>
                                            <p>Monday to Friday: 7:30 AM - 11:30 AM</p>
                                            <p>Saturday & Sunday: 8:00 AM - 9:00 AM</p>
                                            <p>Monday to Friday: 12:00 PM - 5:00 PM</p>
                                            <p>Monday to Saturday: 6:00 PM - 1:00 AM</p>
                                            <p>Sunday to Monday: 5:30 PM - 12:00 AM</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Clear -->
                            <div class="clear"></div>
                        </form>
                    </div><!-- End Contact Form Area -->
                </div><!-- End Inner -->
            </div>
        </section>

        <!-- ============ Social Section  ============= -->
      
        <section class="social_connect">
            <div class="text-content container"> 
                <div class="col-md-6">
                    <span class="social_heading">FOLLOW</span>
                    <ul class="social_icons">
                        <li><a class="icon-twitter color_animation" href="#" target="_blank"></a></li>
                        <li><a class="icon-github color_animation" href="#" target="_blank"></a></li>
                        <li><a class="icon-linkedin color_animation" href="#" target="_blank"></a></li>
                        <li><a class="icon-mail color_animation" href="#"></a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <span class="social_heading">OR DIAL</span>
                    <span class="social_info"><a class="color_animation" href="tel:883-335-6524">(941) 883-335-6524</a></span>
                </div>
            </div>
        </section>

        <!-- ============ Contact Section  ============= -->

        <section id="contact">

            <div  class="contact background_content">
                <h1>Our Featured Dishes <span>Menu</span></h1>
            <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3618.664063989472!2d91.8316103150038!3d24.909437984030877!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x37505558dd0be6a1%3A0x65c7e47c94b6dc45!2sTechnext!5e0!3m2!1sen!2sbd!4v1444461079802" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="inner contact">
                            <!-- Form Area -->
                            <div class="contact-form">
                                <!-- Form -->
                                <form id="contact-us" method="post" action="contact.php">
                                    <!-- Left Inputs -->
                                    <div class="col-md-6 ">
                                        <!-- Name -->
                                        <input type="text" name="name" id="name" required="required" class="form" placeholder="Name" />
                                        <!-- Email -->
                                        <input type="email" name="email" id="email" required="required" class="form" placeholder="Email" />
                                        <!-- Subject -->
                                        <input type="text" name="subject" id="subject" required="required" class="form" placeholder="Subject" />
                                    </div><!-- End Left Inputs -->
                                    <!-- Right Inputs -->
                                    <div class="col-md-6">
                                        <!-- Message -->
                                        <textarea name="message" id="message" class="form textarea"  placeholder="Message"></textarea>
                                    </div><!-- End Right Inputs -->
                                    <!-- Bottom Submit -->
                                    <div class="relative fullwidth col-xs-12">
                                        <!-- Send Button -->
                                        <button type="submit" id="submit" name="submit" class="form-btn">Send Message</button> 
                                    </div><!-- End Bottom Submit -->
                                    <!-- Clear -->
                                    <div class="clear"></div>
                                </form>
                            </div><!-- End Contact Form Area -->
                        </div><!-- End Inner -->
                    </div>
                </div>
            </div>
        </section>


        <!-- ============ Footer Section  ============= -->

        <footer class="sub_footer">
            <div class="container">

              
               @yield('content')

            </div>
        </footer>


        <script type="text/javascript" src="{{asset('jiva/js/jquery-1.10.2.min.js')}}"> </script>
        <script type="text/javascript" src="{{asset('jiva/js/bootstrap.min.js')}}" ></script>
        <script type="text/javascript" src="{{asset('jiva/js/jquery-1.10.2.js')}}"></script>     

        <script type="text/javascript" src="{{asset('jiva/js/main.js')}}" ></script>
        <script type="text/javascript">
            $(document).ready(function(){

                $(".filter-button").click(function(){
                    var value = $(this).attr('data-filter');
                    
                    if(value == "all")
                    {
                        //$('.filter').removeClass('hidden');
                        $('.filter').show('1000');
                    }
                    else
                    {
            //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
            //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                        $(".filter").not('.'+value).hide('3000');
                        $('.filter').filter('.'+value).show('3000');
                        
                    }
                });
                
                if ($(".filter-button").removeClass("active")) {
            $(this).removeClass("active");
            }
            $(this).addClass("active");

            });
        </script>
        <script type="text/javascript" src="{{asset('jiva/js/jquery.mixitup.min.js')}}" ></script>
        <script type="text/javascript" src="{{asset('jiva/js/main.js')}}" ></script>


    </body>
</html>