@extends('PAGEWEB.app')

@section('content')

        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">JIVA</a>
                    @else
                        <a href="{{ route('login') }}">INGRESA</a>
                       
                    @endauth
                </div>
            @endif
        </div>
@endsection

