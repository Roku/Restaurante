<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" id="modal-receta-{{$m->idmenu}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background: #356c8c;">
                <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">MI RECETA</font></h3>
            </div>
            <div class="modal-body">
                
                {{$m->nombre}}
                <div class="row">
                    <div class="col-sm-6">
                        <table>
                            @foreach($detalle_receta as $dtr)
                            @if($dtr->idmenu==$m->idmenu)
                            <tr class="success">
                                <td>{{$dtr->ningrediente}}</td>
                            </tr>
                            
                            @endif

                            @endforeach
                        </table>
                    </div>
                    <div class="col-sm-4">
                        @if(($m->imagen)!="")
                        <img src="{{asset('imagenes/menu/'.$m->imagen)}}"  height="300px" width="400px" style="border-radius: 8px; box-shadow: 2px 4px 6px rgba(0,0,0,0.5)">
                        @endif
                    </div>
                </div>     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Confirmar</button>
            </div>
        </div>
    </div>
</div>