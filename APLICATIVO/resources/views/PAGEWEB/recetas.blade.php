<div id="mixcontent" class="container-galery">
  @foreach($menu as $m)
  <div class="col-md-4 mix filter {{$m->categoria}} no-padding">
    <div class="single_mixi_portfolio">
      @if(($m->imagen)!="")
      <img src="{{asset('imagenes/menu/'.$m->imagen)}}" style="border-radius: 8px; box-shadow: 2px 4px 6px rgba(0,0,0,0.5)">
      @endif
      <div class="mixi_portfolio_overlay">
        <div class="overflow_hover_text">
          <h2>{{$m->nombre}}</h2>
          <a href="" data-target="#modal-receta-{{$m->idmenu}}" data-toggle="modal"><button class="btn btn-warning" >VER +</button></a>
        </div>
      </div>
    </div>
  </div>
  @include('PAGEWEB.modalindex')
  @endforeach
</div>