@extends ('layouts.admin')
@section ('contenido')
<div class="content">
  
<h3 style="text-align:center;color:#30930E; font-weight: bold;">DETALLE DE VENTA DE lA FECHA:{{$venta->fecha_hora}}</h3>
<br>
<div class="row">
  
  <div class="container">
    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-10">
    <div class="form-group">
      <label for="proveedor">Cliente:</label>
      <p>{{$venta->nombre}}</p>
    </div>
  </div>
  <div class="col-lg-3 col-sm-3 col-md-3 col-xs-10">
    <div class="form-group">
      <label for="proveedor">RUC:</label>
      <p>{{$venta->num_documento}}</p>
    </div>
  </div>
  <div class="col-lg-4 col-sm-4 col-md-4 col-xs-10">
    <div class="form-group">
      <label for="proveedor">Dirección:</label>
      <p>{{$venta->direccion}}</p>
    </div>
  </div>
  <div class="col-lg-3 col-sm-3 col-md-3 col-xs-10">
    <div class="form-group">
      <label for="proveedor">Teléfono:</label>
      <p>{{$venta->telefono}}</p>
    </div>
  </div>
  
  <div class="col-lg-3 col-sm-3 col-md-3 col-xs-10">
    <div class="form-group">
      <label for="tipo_comprobante">Comprobante:</label>
      <p>{{$venta->comprobante}}</p>
      
    </div>
  </div>
  <div class="col-lg-3 col-sm-3 col-md-3 col-xs-10">
    <div class="form-group">
      <label for="serie_comprobante">numero Comprobante:</label>
      <p>{{$venta->num_comprobante}}</p>
    </div>
  </div>
  </div>
</div>
<div class="row">
  <div class="panel">
    
    <div class="panel-body">
      
      <div>
        <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
          <thead style="background-color:#A9D0F5 ">
            <tr>
              
              <th>Articulo</th>
              <th>Cantidad</th>
              <th>P.Venta</th>
              
              <th>Subtotal</th>
            </tr>
          </thead>
          
          <tbody>
            @foreach($detalles as $det)
            <tr>
              <td>{{$det->menu}}</td>
              <td>{{$det->cantidad}}</td>
              <td>{{$det->precio_venta}}</td>
              <td>{{ $det->cantidad*$det->precio_venta }}</td>
            </tr>
            
            @endforeach
          </tbody>
          <tfoot>
          <tr>
            
            
      
            <th></th>
            <th></th>
            <th><h4>SubTotal</h4></th>
            <th><h4 id="subtota">$/&nbsp;{{$venta->subtotal_venta}}</h4></th>
          </tr>
          <tr>
            
            

            <th></th>
            <th></th>
            <th><h4>Iva</h4></th>
            <th><h4 id="total">$/&nbsp;{{$venta->iva_venta}}</h4></th>
          </tr>
          <tr>
            
            

            <th></th>
            <th></th>
            <th><h4>TOTAL</h4></th>
            <th><h4 id="total">$/&nbsp;{{$venta->total_venta}}</h4></th>
          </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
  
  
</div>
</div>


@endsection