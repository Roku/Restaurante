@extends ('layouts.admin')
@section ('contenido')
<section id ="pricing" class="description_content" style="padding: 0; margin: 0;">
  <div class="pricing background_content" style="height:125px;">
    <h1 class="top-title-about">Venta Nueva</h1>
  </div>
  <div class="text-content container" style="padding: 0; margin: 0;" >
    <div class="container">
      <div class="row">
        <div class="menu-galery">
          @foreach ($categoria as $c)
          
          <button class="text-center form-btn form-btn btn-default filter-button" data-filter="{{$c->categoria}}">{{$c->nombre}}</button>
          @endforeach
          
        </div>
        
      </div>
    </div>
  </div>
</section>

@include('ventas.venta.ModalAgregar')
@include('flash::message')

<br><br>
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div id="mixcontent" class="container-galery">
        @foreach($menu as $m)
        <div class="col-sm-3 mix filter {{$m->c}} no-padding">
          <div class="single_mixi_portfolio">
            
            @if(($m->imagen)!="")
            <a href="" data-target="#modal-Edit-{{$m->idmenu}}" data-toggle="modal" ><button class="" style="border-style: none;" >
              <img src="{{asset('imagenes/menu/'.$m->imagen)}}" style="border-radius: 10px; box-shadow: 2px 4px 6px rgba(0,0,0,0.5)">
            </button></a>
            @endif
            
          </div>
        </div>


      @include('ventas.venta.modalConfirmar')
        @endforeach
      </div>
    </div>
    
  </div>
</div>
<div class="row">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    
    @if (count($errors)>0)
    <div class="alert alert-danger alert-dismissable  ">
      <button type="button" class="close"  data-dismiss="alert"  >&times;</button>
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
    @endif
  </div>
</div>
<br>
<br>
<div class="container">
  
  <div class="row">
    <!--informacion para factura -->
    {!!Form::open(array('url'=>'ventas/venta','method'=>'POST','autocomplete'=>'off'))!!}
    {{Form::token()}}
    
    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
      <div class="form-group">
        <label for="proveedor">Cliente:</label>
        
        <select name="idcliente" id="pidcliente" class="form-control selectpicker" data-live-search="true">
          @foreach($cliente as $cli)
          <option value="{{$cli->idcliente}}_{{ $cli->num_documento }}_{{ $cli->direccion }}_{{ $cli->telefono }}">{{$cli->nombre}}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
      <div class="form-group">
        <label for="serie_comprobante">Cédula:</label>
        <input type="text" id="pnum_documento"  class="form-control" placeholder="RUC." readonly="">
      </div>
    </div>
    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
      <div class="form-group">
        <label for="serie_comprobante">Dirección:</label>
        <input type="text" id="pdireccion"  class="form-control" placeholder="DIRECCIÓN..." readonly=""  >
      </div>
    </div>
    <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
      <div class="form-group">
        <label for="serie_comprobante">Teléfono:</label>
        <input type="text" id="ptelefono"  class="form-control" placeholder="TELÉFONO..." readonly=""="">
      </div>
    </div>
    
    <div class="col-lg-3 col-sm-4 col-md-4 col-xs-12">
      <div class="form-group">
        <label for="tipo_comprobante">Comprobante:</label>
        
        <select name="idtipo_comprobante" id="tipo_comprobante" class="form-control">
          @foreach($tipo_comprobante as $tip)
          <option value="{{$tip->idtipo_comprobante}}">{{$tip->nombre}}</option>
          @endforeach
        </select>
      </div>
    </div>
    
    <div class="col-lg-3 col-sm-4 col-md-4 col-xs-12">
      <div class="form-group">
        <label for="num_comprobante">Numero Comprobante:</label>
        <input type="text" name="num_comprobante"  class="form-control" placeholder="Numero comprobante..."  style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">
      </div>
    </div>
@foreach($impuesto as $imp)
@endforeach
    <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
      <div class="form-group">
        <label for="serie_comprobante">Iva %:</label>
        <input type="number" id="piva"  class="form-control" placeholder="Iva" readonly=""="" value="{{ $imp->impuesto }}">
      </div>
    </div>
<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
      <div class="form-group">
<br>
      <a href="" data-target="#modal-Create" data-toggle="modal" ><button class="btn btn-primary"  style="height: 40px;" >NUEVO CLIENTE</button></a>
      </div>
    </div>


   
    <!--informacion para factura -->
    <div class="panel panel-primary">
      <div>
        <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
          <thead style="background-color:#A9D0F5 ">
            <tr>
              <th>Opciones</th>
              <th>Articulo</th>
              <th>Cantidad</th>
              <th>P.Venta</th>
              <th>Subtotal</th>
            </tr>
          </thead>
          <tfoot>
          <tr>
            <th  colspan="4"><p align="right">SUBTOTAL:</p></th>
            <th><p id="subtotalfatura">$/. 0.00</p><input type="hidden" name="subtotal_venta" id="subtotal_venta"></th>
          </tr>
          
          <tr>
            <th  colspan="4"><p align="right">IVA:</p></th>
            <th><p id="ivafactura">$/. 0.00</p><input type="hidden" name="iva_venta" id="iva_venta"></th>
          </tr>
          <tr>
            <th  colspan="4"><p align="right">TOTAL:</p></th>
            <th><p id="totalfactura">$/. 0.00</p><input type="hidden" name="total_venta" id="total_venta"></th>
          </tr>
          </tfoot>
        </table>
      </div>
    </div>
    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" id="guardar">
      <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button class="btn btn-primary" type="submit" >Guardar</button>
        <button class="btn btn-danger" type="reset">Cancelar</button>
      </div>
    </div>
  </div>
</div>

{!!Form::close()!!}

@push('scripts')
<script>

$(document).ready(function() {

$('#bt_add').click(function() {
agregar();
});
});
//multipliacaion precio por compra y sale subtotal
var cont=0;
total=0;
subtotal=[];
//variables para que salga cero en el subtotal de cada fila cuando cea cero nuestro descuento
totalDescuento=0;
DescuentoCero=0;
subtotalDescuento=[];
//variables para que salga cero en el subtotal de cada fila cuando cea cero nuestro iva
totalIva=0;
ivaCero=0;
dividido=100;
subtotalIva=[];
//variables para que salga total e mi factura
totalFactura=0;
//variable para el descuento
resultadodescuento=0;
resultadofinal_sumaivadescuento=0;
$("#guardar").hide();
$(document).on('ready',function(){
$('select[name=pidmenu]').val(1);
$('.selectpicker').selectpicker('refresh')
mostrarValores();
});
$(document).on('ready',function(){
$('select[name=pidcliente]').val(1);
$('.selectpicker').selectpicker('refresh')
mostrarValoresCliente();
});
$("#pidmenu").change(mostrarValores);
$("#pidcliente").change(mostrarValoresCliente);
function mostrarValores() {
datosArticulo=document.getElementById('pidmenu').value.split('_');
$("#pprecio_venta").val(datosArticulo[1]);
}
function mostrarValoresCliente() {
datosCliente=document.getElementById('pidcliente').value.split('_');
$("#pnum_documento").val(datosCliente[1]);
$("#pdireccion").val(datosCliente[2]);
$("#ptelefono").val(datosCliente[3]);
}

function agregar(id,nombre,cant,precio,cantidad){
idmenu=id;
articulo=nombre;
cantidad=cant;
precio_venta=precio;
impuesto=$('#piva').val();
//------------------------------------------------------------------------------------------------------------------------
if (idmenu!="" &&cantidad!=""  &&precio_venta!="") {
//--------calculo de subtotal de mi tabla
subtotal[cont]=(cantidad*precio_venta);
total=total+subtotal[cont];
//----calculo del descuento----
subtotalDescuento[cont]=(DescuentoCero);
totalDescuento=totalDescuento+subtotalDescuento[cont];
//--------------------calculo del iva de mi  tabla ---------------
subtotalIva[cont]=(cantidad*precio_venta*impuesto/dividido);
totalIva=totalIva+subtotalIva[cont]
//-------------calculo de total de mi factura------
totalFactura=totalFactura+subtotal[cont]+subtotalIva[cont];

var fila='<tr class="selected" id="fila'+cont+'"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td><td><input type="hidden"  name="idmenu[]" value="'+idmenu+'">'+articulo+'</td><td><input type="number" name="cantidad[]" style="border:none;" readonly value="'+cantidad+'"></td><td><input type="number" style="border:none;" readonly name="precio_venta[]" value="'+parseFloat(precio_venta).toFixed(2)+'"></td><td align="center">S/. '+parseFloat(subtotal[cont]).toFixed(2)+'</td></tr>';
cont++;
limpiar();

//-----------SUBTOTAL DE NUESTRA FACTURA--------
$("#subtotalfatura").html("S/. " + total.toFixed(2));
$("#subtotal_venta").val(total.toFixed(2));
//-----------DESCUENTO DE NUESTRA FACTURA--------

//-----------IVA DE NUESTRA FACTURA--------
$("#ivafactura").html("S/. " + totalIva.toFixed(2));
$("#iva_venta").val(totalIva.toFixed(2));
//-----------TOTAL  DE NUESTRA FACTURA--------
$("#totalfactura").html("S/. " + totalFactura.toFixed(2));
$("#total_venta").val(totalFactura.toFixed(2));
evaluar();
$('#detalles').append(fila);


//-----------------------------------------------------------------------------------------------------------
} else if (idmenu!="" &&cantidad!="" &&precio_venta!="" &&impuesto!="" &&descuento=="0.00") {
if (parseInt(stock)>=parseInt(cantidad))
{
//--------calculo de subtotal de mi tabla
subtotal[cont]=(cantidad*precio_venta);
total=total+subtotal[cont];
//----calculo del descuento----
subtotalDescuento[cont]=(DescuentoCero);
totalDescuento=totalDescuento+subtotalDescuento[cont];
//------------calculo del iva de mi  tabla ---------------
subtotalIva[cont]=(cantidad*precio_venta*impuesto/dividido);
totalIva=totalIva+subtotalIva[cont];
//-------------calculo de total de mi factura------
totalFactura=totalFactura+subtotal[cont]+subtotalIva[cont];

var fila='<tr class="selected" id="fila'+cont+'"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td><td><input type="hidden" name="idmenu[]" value="'+idmenu+'">'+articulo+'</td><td><input type="number" name="cantidad[]" value="'+cantidad+'"></td><td><input type="number" name="precio_venta[]" value="'+parseFloat(precio_venta).toFixed(2)+'"></td><td>'+parseFloat(subtotalIva[cont]).toFixed(2)+'</td><td align="center">S/. '+parseFloat(subtotal[cont]).toFixed(2)+'</td></tr>';
cont++;
limpiar();

//-----------SUBTOTAL DE NUESTRA FACTURA--------
$("#subtotalfatura").html("S/. " + total.toFixed(2));
$("#subtotal_venta").val(total.toFixed(2));
//-----------DESCUENTO DE NUESTRA FACTURA--------
$("#descuentofactura").html("S/. " + totalDescuento.toFixed(2));
$("#descuento_venta").val(totalDescuento.toFixed(2));
//-----------IVA DE NUESTRA FACTURA--------
$("#ivafactura").html("S/. " + totalIva.toFixed(2));
$("#iva_venta").val(totalIva.toFixed(2));
//-----------TOTAL  DE NUESTRA FACTURA--------
$("#totalfactura").html("S/. " + totalFactura.toFixed(2));
$("#total_venta").val(totalFactura.toFixed(2));
evaluar();
$('#detalles').append(fila);
}
else
{
alert ('La cantidad a vender supera el stock');
}
//----------------------------------------------------------------------------------------------------------
} else if (idmenu!="" && cantidad=="" &&stock==""  && precio_venta=="" && descuento!="" && impuesto=="" ) {

if (descuento < total) {
limpiar();

resultadodescuento=total-descuento;
resultadofinal_sumaivadescuento=resultadodescuento+totalIva;

//-----------SUBTOTAL DE NUESTRA FACTURA--------
$("#subtotalfatura").html("S/. " + total.toFixed(2));
$("#subtotal_venta").val(total.toFixed(2));
//-----------DESCUENTO DE NUESTRA FACTURA--------
$("#descuentofactura").html("$/. " +descuento);
$("#descuento_venta").val(descuento);
//-----------IVA DE NUESTRA FACTURA--------
$("#ivafactura").html("S/. " + totalIva.toFixed(2));
$("#iva_venta").val(totalIva.toFixed(2));
//-----------TOTAL  DE NUESTRA FACTURA--------
$("#totalfactura").html("S/. " + resultadofinal_sumaivadescuento.toFixed(2));
$("#total_venta").val(resultadofinal_sumaivadescuento.toFixed(2));
evaluar();
$('#detalles').append(fila);
}else {

alert("EL DESCUENTO SUPERA AL TOTAL DE LA VENTA");
}

}else {
alert("REVISA LOS DATOS DE LA FACTURA")
}
}
function limpiar(){
$("#pcantidad").val("");
$("#pprecio_compra").val("");
$("#pprecio_venta").val("");
$("#pstock").val("");
$("#pidimpuesto").val("");
}
function evaluar(){
if (total>0) {
$("#guardar").show();
} else{
$("#guardar").hide();
}
}
function eliminar(index) {

//remover el subtotal de mi factura
total=total-subtotal[index];
$("#subtotalfatura").html("S/. " + total.toFixed(2));
$("#subtotal_venta").val(total.toFixed(2));
$("#fila" + index).remove();
//remover el iva
totalIva=totalIva-subtotalIva[index];
$("#ivafactura").html("S/. " + totalIva.toFixed(2));
$("#iva_venta").val(totalIva.toFixed(2));
$("#fila" + index).remove();
//remover el total de la venta
totalFactura=totalFactura-subtotalIva[index]-subtotal[index];
$("#totalfactura").html("S/. " + totalFactura.toFixed(2));
$("#total_venta").val(totalFactura.toFixed(2));
$("#fila" + index).remove();
evaluar();
}
</script>
@endpush
@endsection
