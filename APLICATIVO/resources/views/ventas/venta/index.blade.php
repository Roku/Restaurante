@extends('layouts.admin')
@section('contenido')
@include('flash::message')

<div class="content">
  <div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<br>
  <h3>LISTADO DE VENTA.<a href="{{ URL('ventas/venta/create') }}"><button class="btn btn-success"> <i class="fa fa-pencil"> NUEVO </i></button></a> <a href="{{ URL('reportecompras') }}"><button class="btn btn-warning"><i class="fa fa-file-text-o"> REPORTE</i></button> </a></h3>
  
 </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="table-responsive">
       <table id="tabla" class="table  table-hover">
<thead class="fondo">
      <tr>
        <th>CLIENTE</th>
        <th>COMPROBANTE</th>
         <th>FECHA</th>   
         <th>TOTAL</th>
        <th>ESTADO</th>
        <th>OPCIONES</th>
      </tr>
    </thead>
   <tbody> 
    @foreach($venta as $ven)
      <tr>

        <td>{{ $ven->nombre }}</td>
         <td>{{$ven->num_comprobante}}</td>
        <td>{{$ven->fecha_hora}}</td>
        <td>{{$ven->total_venta}}</td>
        <td>{{$ven->estado}}</td>
       
      
          <td>
          
            <a href="{{URL::action('VentaController@show',$ven->idventa)}}" ><button class="btn btn-primary" title="VER DETALLE" ><i class="fa fa-eye"></i></button></a>
              <a href="" data-target="#modal-delete-{{$ven->idventa}}" data-toggle="modal" ><button class="btn btn-danger" title=" ANULAR" ><i class="fa fa-times-circle"></i></button></a>
           
        </td>
      </tr>
   

    @endforeach
    </tbody>
   </table>
</div>
  </div>
</div>
</div>

@endsection


