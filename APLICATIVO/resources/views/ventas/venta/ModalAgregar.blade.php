<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" id="modal-Create">
	{{Form::Open(array('action'=>array('VentaController@cliente'),'method'=>'GET'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #356c8c;">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> <font color="FBFBEF">CREAR CLIENTE</font></h3>
			</div>
			<div class="modal-body">
				
{!!Form::token()!!}

<div class="row"> 
<div class="col-sm-6">  
 <div class="form-group">
    <label for="nombre">Nombre: </label>
    <input type="text" name="nombre" class="form-control" placeholder="Nombre..." style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">  
    </div>
</div>
<div class="col-sm-6">
   <div class="form-group">
    <label for="nombre">Apellido: </label>
    <input type="text" name="apellido" class="form-control" placeholder="Nombre..." style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">  
    </div>
  </div>
</div>
    
 
     <div class="form-group">
    <label for="nombre">Cédula: </label>
    <input type="text" name="num_documento" class="form-control" placeholder="Nombre..." style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">	
    </div>
     <div class="form-group">
    <label for="descripcion">Teléfono: </label>
    <input type="text" name="telefono" class="form-control" placeholder="Descripcion..." style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">	
    </div>
      <div class="form-group">
    <label for="descripcion">Dirección: </label>
    <input type="text" name="direccion" class="form-control" placeholder="Descripcion..." style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">	
    </div>
      <div class="form-group">
    <label for="descripcion">Email: </label>
    <input type="text" name="email" class="form-control" placeholder="Descripcion..." style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">	
    </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
