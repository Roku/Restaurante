<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'JIVA') }}</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style>
        *{
        margin: 0;
        padding: 0;
        overflow-x: hidden;
        overflow-y: hidden;
        }
        .secction-slider-image{
        width: 35%;
        min-height:720px;
        height: 100%;
        padding: 0;
        margin:0;
        float: left;
        
        }
        .secction-slider-login{
        width: 65%;
        min-height:720px;
        height: 100%;
        padding: 0;
        margin:0;
        float: right;
        overflow-x: hidden;
        
        }
        .carousel .slide{
        margin:0;
        padding:0;
        }
        
        .item img{
        min-height:670px;
        height: 100%;
        }
        
        </style>
    </head>
    <body>
        <div class="container-login">
            <div class="row">
                <div class="col-sm-2 secction-slider-image">
                    <ul class="image_box_story2">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="images/slider1.jpg" >
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="images/slider2.jpg">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="images/slider3.JPG">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
                <div class="col-sm-6 secction-slider-login" style="background-color:#fff;">
                    @yield('content')
                </div>
            </div>
        </div>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>