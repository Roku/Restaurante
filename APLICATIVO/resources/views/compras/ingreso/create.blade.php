@extends ('layouts.admin')
@section ('contenido')
<h3 style="text-align:center;color:#30930E; font-weight: bold;">NUEVA COMPRA</h3>
  <div class="row">

    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      
      @if (count($errors)>0)
    <div class="alert alert-danger alert-dismissable  ">
  <button type="button" class="close"  data-dismiss="alert"  >&times;</button>
        <ul>
        @foreach ($errors->all() as $error)
          <li>{{$error}}</li>
        @endforeach
        </ul>
      </div>
      @endif
    </div> 
  </div> 
  {!!Form::open(array('url'=>'compras/ingreso','method'=>'POST','autocomplete'=>'off'))!!}
    {{Form::token()}}

<div class="content">
<div class="row"> 
<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
        <div class="form-group">
              <label for="proveedor">Proveedor:</label>
              
              <select name="idproveedor" id="pidproveedor" class="form-control selectpicker" data-live-search="true">
              @foreach($proveedor as $prove)
                <option value="{{$prove->idproveedor}}_{{ $prove->num_documento }}_{{ $prove->direccion }}_{{ $prove->telefono }}">{{$prove->empresa}}</option>
                @endforeach
              </select>
            </div>
    </div>

      <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
               <div class="form-group">
              <label for="serie_comprobante">RUC de la Empresa:</label>
              <input type="text" id="pnum_documento"  class="form-control" placeholder="RUC." disabled>
            </div>
      </div>
 <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
               <div class="form-group">
              <label for="serie_comprobante">Dirección:</label>
              <input type="text" id="pdireccion"  class="form-control" placeholder="DIRECCIÓN..." disabled  >
            </div>
      </div>
       <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
               <div class="form-group">
              <label for="serie_comprobante">Teléfono:</label>
              <input type="text" id="ptelefono"  class="form-control" placeholder="TELÉFONO..." disabled="">
            </div>
      </div>

    
      <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
        <div class="form-group">
              <label for="tipo_comprobante">Comprobante:</label>
              
              <select name="idtipo_comprobante" id="tipo_comprobante" class="form-control">
              @foreach($tipo_comprobante as $tip)
                <option value="{{$tip->idtipo_comprobante}}">{{$tip->nombre}}</option>
                @endforeach
              </select>
            </div>
    </div>


    
     <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
       <div class="form-group">
              <label for="num_comprobante">Numero Comprobante:</label>
              <input type="text" name="num_comprobante"  class="form-control" placeholder="Numero comprobante..."  style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">
            </div>
      </div>
      <br>
<div class="panel">
  
  <div class="panel-body">
         <div class="col-lg-5 col-sm-6 col-md-6 col-xs-12">
                <div class="form-group">
              <label >Articulo:</label>
              
              <select name="pidarticulo" id="pidarticulo" class="form-control selectpicker" data-live-search="true">
              @foreach($ingrediente as $ing)
                <option value="{{$ing->idingrediente}}_{{ $ing->impuesto }}">{{$ing->ingrediente}}</option>
                @endforeach
              </select>
            </div>
    </div>
  
  

  <div class="col-lg-1 col-sm-1 col-md-1 col-xs-12">
    <div class="form-group">
      <label for="cantidad">Cantidad</label>
      <input type="number" name="pcantidad" id="pcantidad" class="form-control" placeholder="0.00" >

    </div>

    
  </div>
   <div class="col-lg-1 col-sm-1 col-md-1 col-xs-12">
                <div class="form-group">
              <label >medida:</label>
              
              <select name="pmedida" id="pmedida" class="form-control selectpicker" data-live-search="true">
              @foreach($medida as $med)
                <option value="{{$med->idunidad_medida}}">{{$med->nombre}}</option>
                @endforeach
              </select>
            </div>
    </div>
 <div class="col-lg-1 col-sm-1 col-md-1 col-xs-12">
  <div class="form-group">
    <label>P.Compra</label>

    <input type="number" name="pprecio_compra" id="pprecio_compra" class="form-control" placeholder="0.00">

  </div>
    
  </div>
<div class="col-lg-1 col-sm-1 col-md-1 col-xs-12">
     <div class="form-group">
       
    <label>P.Venta</label>
    <input type="number" name="pprecio_venta" id="pprecio_venta" class="form-control" placeholder="0.00">


     </div>


  </div>
  <div class="col-lg-1 col-sm-1 col-md-1 col-xs-12">
     <div class="form-group">
       
    <label>IVA</label>
    <input type="enable" name="pidimpuesto" id="pidimpuesto" class="form-control" readonly="readonly" placeholder="0.00" >


     </div>
  </div>
  <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
     
     <div class="form-group">
       <br> 
       <button type="button" id="bt_add" class="btn btn-success" style="width: 140px; height: 39px;">Agregar</button>

     </div>



   </div>

<div>
   <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
     <thead style="background-color:#A9D0F5 ">
       <tr>
         <th>Opciones</th>
         <th>Articulo</th>
         <th>Cantidad</th>
         <th>Medida</th>
         <th>P.Compra</th>
         <th>P.Venta</th>
         <th>Iva</th>
         <th>Subtotal</th>
       </tr>
     </thead>
     <tfoot>
       <tr>
        <th  colspan="7"><p align="right">SUBTOTAL:</p></th>
         <th><h4 id="subtota">$/. 0.00</h4><input type="hidden" name="subtotal_compra" id="subtotal_compra"></th>
       </tr>
           <tr>
            <th  colspan="7"><p align="right">IVA:</p></th>
         <th><h4 id="iva">$/. 0.00</h4><input type="hidden" name="iva_compra" id="iva_compra"></th>
       </tr>
           <tr>
             <th  colspan="7"><p align="right">TOTAL:</p></th>
   <th><h4 id="total">$/. 0.00</h4><input type="hidden" name="total_compra" id="total_compra"></th>
       </tr>
     </tfoot>
   </table>
</div>
</div>

</div>

    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12" id="guardar">
      <div class="form-group">

      <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button class="btn btn-primary" type="submit" >Guardar</button>
              <button class="btn btn-danger" type="reset">Cancelar</button>
            </div>
    </div>
            
</div>
</div>
      {!!Form::close()!!}   
         

 @push('scripts')

<script>
  
  $(document).ready(function() {
   
    $('#bt_add').click(function() {

agregar();

    });    

  });

var cont=0;
  total=0;
  subtota=0;
  iva=0;
  totaliva=[];
  subtotall=[];
  subtotal=[];
  prueba=0;
  prue=0;
  uno=0;
  cero=0;
  pu=0;
  ivanumero=0;
  resultadoiva=0;
ivaporcentaje=100;
  $("#guardar").hide();
   $(document).on('ready',function(){
  $('select[name=pidproveedor]').val(1);
  $('.selectpicker').selectpicker('refresh')
  mostrarValoresProveedor();
});
    $(document).on('ready',function(){
  $('select[name=pidarticulo]').val(1);
  $('.selectpicker').selectpicker('refresh')
  mostrarValores();
});
  $("#pidarticulo").change(mostrarValores);
    $("#pidproveedor").change(mostrarValoresProveedor);
function mostrarValores() {
  datosArticulo=document.getElementById('pidarticulo').value.split('_');
  $("#pidimpuesto").val(datosArticulo[1]);
}
function mostrarValoresProveedor() {
  datosProveedor=document.getElementById('pidproveedor').value.split('_');
  $("#pnum_documento").val(datosProveedor[1]);
   $("#pdireccion").val(datosProveedor[2]);
   $("#ptelefono").val(datosProveedor[3]);

}

function agregar(){

idingrediente=$("#pidarticulo").val();
articulo=$("#pidarticulo option:selected").text();
cantidad=$("#pcantidad").val();
idunidad_medida=$("#pmedida").val();
medida=$("#pmedida option:selected").text();
precio_compra=$("#pprecio_compra").val();
precio_venta=$("#pprecio_venta").val();
impuesto=$("#pidimpuesto").val();

if (idingrediente!="" && cantidad>0 && precio_compra!="" && precio_venta!="" && impuesto=="0.00" &&precio_venta>precio_compra) {
 

 subtotall[cont]=(cantidad*precio_compra);
  subtotal[cont]=(cantidad*precio_compra);
  totaliva[cont]=(uno*cero);

  iva=iva+totaliva[cont];


 total=total+subtotal[cont];
 subtota=subtota+subtotall[cont];

 var fila='<tr class="select" id="fila'+cont+'"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td><td><input type="hidden" name="idingrediente[]" value="'+idingrediente+'">'+articulo+'</td><td><input type="number" style="border:none;" readonly name="cantidad[]" value="'+cantidad+'" readonly></td><td><input type="hidden" name="idunidad_medida[]" value="'+idunidad_medida+'">'+medida+'</td><td><input type="number" style="border:none;" readonly name="precio_compra[]" value="'+parseFloat(precio_compra).toFixed(2)+'" readonly></td><td><input type="number" name="precio_venta[]" style="border:none;" readonly value="'+parseFloat(precio_venta).toFixed(2)+'" readonly></td><td>'+totaliva[cont]+'</td><td align="center">S/. '+parseFloat(subtotall[cont]).toFixed(2)+'</td></tr>';
 cont++;

 limpiar();
 $("#iva").html("$/. " +iva.toFixed(2));
 $("#iva_compra").val(iva.toFixed(2));
$("#subtota").html("$/. " +subtota.toFixed(2));
 $("#subtotal_compra").val(subtota.toFixed(2));
 $("#total").html("$/. " +total.toFixed(2));
 $("#total_compra").val(total.toFixed(2));
evaluar();

$('#detalles').append(fila);

}

else if (idingrediente!="" && cantidad>0 && precio_compra!="" && precio_venta!="" && impuesto!="" &&precio_venta>precio_compra) {
 subtotall[cont]=(cantidad*precio_compra);
 prueba=(cantidad*precio_compra*impuesto/ivaporcentaje);
 subtotal[cont]=(cantidad*precio_compra+prueba);
ivanumero=(cantidad*precio_compra*impuesto/ivaporcentaje);
totaliva[cont]=(ivanumero);
  
 total=total+subtotal[cont];

subtota=subtota+subtotall[cont];

iva=iva+totaliva[cont];


 var fila='<tr class="select" id="fila'+cont+'"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td><td><input type="hidden" name="idingrediente[]" value="'+idingrediente+'">'+articulo+'</td><td><input type="number" name="cantidad[]" style="border:none;" readonly value="'+cantidad+'"></td><td><input type="hidden" name="idunidad_medida[]" value="'+idunidad_medida+'">'+medida+'</td><td><input type="number" style="border:none;" readonly name="precio_compra[]" value="'+parseFloat(precio_compra).toFixed(2)+'"></td><td><input type="number" style="border:none;" readonly name="precio_venta[]" value="'+parseFloat(precio_venta).toFixed(2)+'"></td><td>'+parseFloat(totaliva[cont]).toFixed(2)+'</td><td align="center">S/. '+parseFloat(subtotall[cont]).toFixed(2)+'</td></tr>';
 cont++;

 limpiar();
 $("#iva").html("$/. " +iva.toFixed(2));
 $("#iva_compra").val(iva.toFixed(2));
$("#subtota").html("$/. " +subtota.toFixed(2));
 $("#subtotal_compra").val(subtota.toFixed(2));
 $("#total").html("$/. " +total.toFixed(2));
 $("#total_compra").val(total.toFixed(2));
evaluar();

$('#detalles').append(fila);

}

else{

alert("Error al ingresar el detalle de ingreso,revise los datos del artículo");


} 



}


function limpiar(){

$("#pcantidad").val("");
$("#pprecio_compra").val("");
$("#pprecio_venta").val("");



}

function evaluar(){

if (total>0) {

$("#guardar").show();



} else{

$("#guardar").hide();


}


}

function eliminar(index) {
  
 iva=iva-totaliva[index];
  $("#iva").html("$/. " +iva.toFixed(2));
 $("#iva_compra").val(iva.toFixed(2));
  $("#fila" + index).remove();



   subtota=subtota-subtotall[index];
  $("#subtota").html("$/. " +subtota.toFixed(2));
 $("#subtotal_compra").val(subtota.toFixed(2));
  $("#fila" + index).remove();




  total=total-subtotal[index];
 
 $("#total").html("$/. " +total.toFixed(2));
 $("#total_compra").val(total.toFixed(2));
  $("#fila" + index).remove();
  evaluar();







}


</script>

 @endpush



@endsection