@extends('layouts.admin')
@section('contenido')

@include('flash::message')

<div class="content">
  <div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<br>
  <h3>LISTADO DE COMPRAS.<a href="{{ URL('compras/ingreso/create') }}"><button class="btn btn-success"> <i class="fa fa-pencil"> NUEVO </i></button></a> <a href="{{ URL('reportecompras') }}"><button class="btn btn-warning"><i class="fa fa-file-text-o"> REPORTE</i></button> </a></h3>
  
 </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="table-responsive">
       <table id="tabla" class="table  table-hover">
<thead class="fondo">
      <tr>
        <th>PROVEEDOR</th>
        <th>COMPROBANTE</th>
         <th>FECHA</th>   
         <th>TOTAL</th>
        <th>ESTADO</th>
        <th>OPCIONES</th>
      </tr>
    </thead>
   <tbody> 
    @foreach($ingreso as $ing)
      <tr>

        <td>{{ $ing->proveedor }}</td>
         <td>{{$ing->nombre.'-'.$ing->num_comprobante}}</td>
        <td>{{$ing->fecha_hora}}</td>
        <td>{{$ing->total_compra}}</td>
        <td>{{$ing->estado}}</td>
       
      
          <td>
          
            <a href="{{URL::action('IngresoController@show',$ing->idingreso)}}" ><button class="btn btn-primary" title="VER DETALLE" ><i class="fa fa-eye"></i></button></a>
              <a href="" data-target="#modal-delete-{{$ing->idingreso}}" data-toggle="modal" ><button class="btn btn-danger" title=" ANULAR" ><i class="fa fa-times-circle"></i></button></a>
           
        </td>
      </tr>
   

    @endforeach
    </tbody>
   </table>
</div>
  </div>
</div>
</div>

@endsection


